-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-optimizations optimization_filter
-allowaccessmodification

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keep public class android.support.v7.widget.** { *; }
-keep public class android.support.v7.internal.widget.** { *; }
-keep public class android.support.v7.internal.view.menu.** { *; }

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembernames class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}


-keepclassmembers class fqcn.of.javascript.interface.for.webview {
  public *;
}

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

-keep public class android.support.v7.preference.Preference {
  public <init>(android.content.Context, android.util.AttributeSet);
}
-keep public class * extends android.support.v7.preference.Preference {
  public <init>(android.content.Context, android.util.AttributeSet);
}

-keep public class * extends android.support.v4.view.ActionProvider {
    public <init>(android.content.Context);
}


-dontwarn android.support.
-dontwarn com.google.ads.
-dontwarn com.vungle.**
-dontnote com.vungle.**
-dontwarn okio.**
-dontwarn com.comscore.**
-dontwarn com.moat.**
-dontwarn im.delight.**
-dontwarn org.apache.http.**
-dontwarn android.webkit.**
-dontwarn com.squareup.okhttp.**
-dontwarn com.daimajia.slider.**
-dontwarn com.squareup.okhttp.**
-dontwarn retrofit2.**
-dontwarn org.codehaus.mojo.**
-dontwarn org.androidannotations.api.rest.**

-keep class android.support.v4.app.* { *; }
-keep class com.vungle.** { *; }
-keep class javax.inject.*
-keep class org.apache.http.** { *; }
-keep class org.apache.commons.codec.** { *; }
-keep class org.apache.commons.logging.** { *; }
-keep class android.net.compatibility.** { *; }
-keep class android.net.http.** { *; }
-keep class com.daimajia.slider.** { *; }
-keep class retrofit2.** { *; }
-keep class retrofit.** { *; }
-keep class retrofit.http.** { *; }
-keep class retrofit.client.** { *; }
-keep class com.squareup.okhttp.** { *; }
-keep class com.example.package.network.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-keep interface org.apache.http.**
-keep interface android.support.v4.app*. { *; }
-keep class com.loopj.android.** { *; }
-keep interface com.loopj.android.** { *; }
-keep class com.loopj.android.** { *; }
-keep interface com.loopj.android.** { *; }
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep class org.sqlite.** { *; }
-keep class org.sqlite.database.** { *; }
-keep class android.content.** { *; }
-keep class com.wang.avi.** { *; }
-keep class com.wang.avi.indicators.** { *; }

-keepattributes Signature
-keepattributes Exceptions
-keepattributes *Annotation*
-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations
-keepattributes EnclosingMethod

