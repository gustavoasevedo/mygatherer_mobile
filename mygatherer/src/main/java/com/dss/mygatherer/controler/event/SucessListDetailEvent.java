package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.CardsList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessListDetailEvent {
    private CardsList cardsList;
    private boolean sucess;
    private String message;

    public SucessListDetailEvent(@Nullable CardsList cardsList, boolean sucess, String message) {
        this.cardsList = cardsList;
        this.sucess = sucess;
        this.message = message;
    }

    public CardsList getCardsList() {
        return cardsList;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
