package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessBannedListEvent {
    private ArrayList<String> cards;
    private boolean sucess;
    private String message;
    private String idFormat;

    public SucessBannedListEvent(@Nullable ArrayList<String> sets, boolean sucess, String message, String idFormat) {
        this.cards = sets;
        this.sucess = sucess;
        this.message = message;
        this.setIdFormat(idFormat);
    }

    public ArrayList<String> getCards() {
        return cards;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdFormat() {
        return idFormat;
    }

    public void setIdFormat(String idFormat) {
        this.idFormat = idFormat;
    }
}
