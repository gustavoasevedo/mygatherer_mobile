package com.dss.mygatherer.controler.async;

import com.dss.mygatherer.model.CardSet;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

@EBean
public class AutoCompleteList {

    public static String setHeaderJson(String header, String json) {
        return "{" + '"' + header + '"' + ":" + json + "}";
    }

    @SerializedName("CardSet")
    public ArrayList<CardSet> list = new ArrayList<>();

}
