package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.Format;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessFormatEvent {
    private ArrayList<Format> formats;
    private boolean sucess;
    private String message;
    private int screen;

    public SucessFormatEvent(@Nullable ArrayList<Format> formats, boolean sucess, String message, int screen) {
        this.formats = formats;
        this.sucess = sucess;
        this.message = message;
        this.setScreen(screen);
    }

    public ArrayList<Format> getFormats() {
        return formats;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getScreen() {
        return screen;
    }

    public void setScreen(int screen) {
        this.screen = screen;
    }
}
