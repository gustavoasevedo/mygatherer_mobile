package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.CardSet;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessCardsSearchEvent implements Serializable{
    private ArrayList<CardSet> cardsLists;
    private boolean sucess;
    private String message;

    public SucessCardsSearchEvent(@Nullable ArrayList<CardSet> cardsLists, boolean sucess, String message) {
        this.cardsLists = cardsLists;
        this.sucess = sucess;
        this.message = message;
    }

    public ArrayList<CardSet> getCardsLists() {
        return cardsLists;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
