package com.dss.mygatherer.controler.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.dss.mygatherer.model.CardSet;
import com.dss.mygatherer.tools.HttpUtil;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 03/04/2018.
 */

public class AutoCompleteAsyncTask extends AsyncTask<Void, Integer, Boolean> {
    private static final String TAG = "CepAsyncTask";

    Context context;
    String name;
    CallbackAutoComplete callback;
    ArrayList<CardSet> cards;

    public AutoCompleteAsyncTask(Context context, String name, CallbackAutoComplete callback) {
        this.context = context;
        this.name = name;
        this.callback = callback;
    }

    @Override
    protected Boolean doInBackground(Void... params) {
        JSONObject jsonObject = new JSONObject();
        try {
            String result = makeConnection();

            if (result != null && !result.equals("[]") && !result.equals("null")) {
                decompodeJson(result);
                return true;
            }

        } catch(Exception e){
            return false;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean sucesso) {
        Log.i(TAG, "Sucesso: " + sucesso);
        callback.onAutoCompleteReturn(cards);

    }


    public String makeConnection() throws IOException, JSONException {
        String url = "http://mygatherer.xyz/" + "api/json/AutoCompleteJson.php";
        url += "?name=" + name;
        String result = HttpUtil.getExecute(url, context);

        return result;
    }


    public void decompodeJson(String result) throws JSONException {
        Gson serializer = new Gson();

        AutoCompleteList cards = serializer.fromJson(
                AutoCompleteList.setHeaderJson("CardSet", result), AutoCompleteList.class);

        this.cards = cards.list;
    }
}
