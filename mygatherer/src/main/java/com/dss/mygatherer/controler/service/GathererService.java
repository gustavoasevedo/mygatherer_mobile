package com.dss.mygatherer.controler.service;

import com.dss.mygatherer.model.CardSet;
import com.dss.mygatherer.model.CardsList;
import com.dss.mygatherer.model.DefaultReturn;
import com.dss.mygatherer.model.Format;
import com.dss.mygatherer.model.Planechase;
import com.dss.mygatherer.model.Price;
import com.dss.mygatherer.model.Set;
import com.dss.mygatherer.model.Type;
import com.dss.mygatherer.model.User;
import com.dss.mygatherer.model.cardsDetails.CardData;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public interface GathererService {

    @FormUrlEncoded
    @POST("api/json/loginUser.php")
    Call<List<User>> login(
            @Field("login") String login,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("api/json/cadastroUsuario.php")
    Call<DefaultReturn> createUser(
            @Field("email") String email,
            @Field("password") String password,
            @Field("nameuser") String nameuser,
            @Field("nickname") String nickname
    );

    @FormUrlEncoded
    @POST("api/json/trocaSenha.php")
    Call<DefaultReturn> changePassword(
            @Field("idUser") int idUser,
            @Field("oldPass") String oldPass,
            @Field("newPass") String newPass
    );

    @FormUrlEncoded
    @POST("api/json/esqueciSenha.php")
    Call<DefaultReturn> forgotPassword(
            @Field("email") String email
    );

    @FormUrlEncoded
    @POST("api/json/listJson.php")
    Call<DefaultReturn> createOrChangeList(
            @Field("json_list") String jsonList
    );

    @FormUrlEncoded
    @POST("api/json/deleteListJson.php")
    Call<DefaultReturn> deleteList(
            @Field("idlist") int idlist
    );

    @GET("api//json/searchListsJson.php")
    Call<List<CardsList>> getListCards(@Query("id") int id);

    @GET("api/json/SetsJson.php")
    Call<List<Set>> getAllSets();

    @GET("api/json/FormatsJson.php")
    Call<List<Format>> getAllFormats();

    @GET("api/json/TypesJson.php")
    Call<List<Type>> getAllTypes();

    @GET("api//json/PlanechaseCardsJson.php")
    Call<List<Planechase>> getPlanechaseCards();

    @GET("api/json/BannedCardsJson.php")
    Call<List<String>> getBannedsFormat(@Query("idFormat") int idFormat);

    @GET("api/json/SetCardJson.php")
    Call<List<CardSet>> getCardsSet(@Query("set") String set);

    @GET
    Call<List<CardSet>> filterResult(@Url String url);

    @GET("api/json/CardsDataJson.php")
    Call<CardData> getCardInfo(@Query("name") String name);

    @GET("api/json/CardPriceJson.php")
    Call<Price> getCardPrice(@Query("cardName") String cardName, @Query("cardSetCode") String cardSetCode);

    @GET("api//json/listDetailJson.php")
    Call<CardsList> getListDetail(@Query("id") int id);
}
