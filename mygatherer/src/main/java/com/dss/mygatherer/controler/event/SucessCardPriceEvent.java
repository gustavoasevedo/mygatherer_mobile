package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.Price;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessCardPriceEvent {
    private Price price;
    private boolean sucess;
    private String message;

    public SucessCardPriceEvent(@Nullable Price price, boolean sucess, String message) {
        this.price = price;
        this.sucess = sucess;
        this.message = message;
    }

    public Price getPrice() {
        return price;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
