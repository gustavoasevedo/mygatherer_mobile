package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.Type;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessTypesEvent {
    private ArrayList<Type> types;
    private boolean sucess;
    private String message;

    public SucessTypesEvent(@Nullable ArrayList<Type> types, boolean sucess, String message) {
        this.types = types;
        this.sucess = sucess;
        this.message = message;
    }

    public ArrayList<Type> getCardsLists() {
        return types;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
