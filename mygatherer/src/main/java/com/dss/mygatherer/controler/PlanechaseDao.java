package com.dss.mygatherer.controler;

import android.content.Context;

import com.dss.mygatherer.model.Planechase;
import com.dss.mygatherer.tools.ConstantDB;
import com.dss.sdatabase.dao.BaseTable;
import com.dss.sdatabase.exceptions.InvalidTypeException;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 08/05/2017.
 */

public class PlanechaseDao extends BaseTable {

    private static PlanechaseDao instance;
    private Context context;

    public PlanechaseDao(Context context) {
        super(context, Planechase.class, ConstantDB.DB_NAME, ConstantDB.DB_VERSION);
        this.context = context;
        createTable();
    }

    public static PlanechaseDao getInstance(Context context) {
        if (instance == null) {
            synchronized (PlanechaseDao.class) {
                if (instance == null) {
                    instance = new PlanechaseDao(context);
                }
            }
        }
        return instance;
    }

    public void insertObject(Planechase foto) {

        ArrayList<Object> objectArrayList = new ArrayList<>();

        objectArrayList.add(foto);

        insert(objectArrayList);
    }

    public ArrayList<Planechase> selectAllCards() {

        ArrayList<Object> objectList = new ArrayList<>();
        ArrayList<Planechase> fotos = new ArrayList<>();

        objectList = selecList();

        fotos = mountObjectList(objectList);

        return fotos;

    }

    public void deleteId(String Id) {

        String[] values = {String.valueOf(Id)};

        deleteAll("IdAnuncio = ?", values);


    }

    public ArrayList<Object> selecList() {

        ArrayList<Object> list = new ArrayList<>();

        try {

            list = selectList();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvalidTypeException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Planechase> mountObjectList(ArrayList<Object> objectList) {

        ArrayList<Planechase> lista = new ArrayList<>();

        for (Object object : objectList) {
            Planechase foto = ((Planechase) object);
            lista.add(foto);
        }

        return lista;
    }

    public void deleteAllItems() {
        deleteAll();
    }
}
