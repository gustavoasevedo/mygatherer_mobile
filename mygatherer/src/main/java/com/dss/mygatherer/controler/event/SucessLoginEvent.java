package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.User;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessLoginEvent {
    private User user;
    private boolean sucess;
    private String message;

    public SucessLoginEvent(@Nullable User user, boolean sucess, String message) {
        this.user = user;
        this.sucess = sucess;
        this.message = message;
    }

    public User getUser() {
        return user;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
