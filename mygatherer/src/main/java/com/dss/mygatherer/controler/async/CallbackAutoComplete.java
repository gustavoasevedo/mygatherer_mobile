package com.dss.mygatherer.controler.async;

import com.dss.mygatherer.model.CardSet;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 13/06/2018.
 */
public interface CallbackAutoComplete {
    void onAutoCompleteReturn(ArrayList<CardSet> cards);
}
