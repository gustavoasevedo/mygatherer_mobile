package com.dss.mygatherer.controler.event;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class RequestErrorEvent {

    private String message;

    public RequestErrorEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
