package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.Set;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessSetsEvent {
    private ArrayList<Set> sets;
    private boolean sucess;
    private String message;
    private int screen;

    public SucessSetsEvent(@Nullable ArrayList<Set> sets, boolean sucess, String message, int screen) {
        this.sets = sets;
        this.sucess = sucess;
        this.message = message;
        this.setScreen(screen);
    }

    public ArrayList<Set> getSets() {
        return sets;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getScreen() {
        return screen;
    }

    public void setScreen(int screen) {
        this.screen = screen;
    }
}
