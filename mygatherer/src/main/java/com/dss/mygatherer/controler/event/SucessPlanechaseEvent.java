package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.Planechase;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessPlanechaseEvent {
    private ArrayList<Planechase> planechases;
    private boolean sucess;
    private String message;

    public SucessPlanechaseEvent(@Nullable ArrayList<Planechase> planechases, boolean sucess, String message) {
        this.planechases = planechases;
        this.sucess = sucess;
        this.message = message;
    }

    public ArrayList<Planechase> getPlanechases() {
        return planechases;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
