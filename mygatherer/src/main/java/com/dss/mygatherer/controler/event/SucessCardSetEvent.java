package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.CardSet;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessCardSetEvent {
    private ArrayList<CardSet> cards;
    private boolean sucess;
    private String message;

    public SucessCardSetEvent(@Nullable ArrayList<CardSet> cards, boolean sucess, String message) {
        this.cards = cards;
        this.sucess = sucess;
        this.message = message;
    }

    public ArrayList<CardSet> getCards() {
        return cards;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
