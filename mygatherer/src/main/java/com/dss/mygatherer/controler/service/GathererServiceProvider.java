package com.dss.mygatherer.controler.service;

import com.dss.mygatherer.controler.event.SucessCardPriceEvent;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessBannedListEvent;
import com.dss.mygatherer.controler.event.SucessCardDetailEvent;
import com.dss.mygatherer.controler.event.SucessCardSetEvent;
import com.dss.mygatherer.controler.event.SucessCardsListEvent;
import com.dss.mygatherer.controler.event.SucessCardsSearchEvent;
import com.dss.mygatherer.controler.event.SucessDefaultEvent;
import com.dss.mygatherer.controler.event.SucessFormatEvent;
import com.dss.mygatherer.controler.event.SucessListDetailEvent;
import com.dss.mygatherer.controler.event.SucessLoginEvent;
import com.dss.mygatherer.controler.event.SucessPlanechaseEvent;
import com.dss.mygatherer.controler.event.SucessSetsEvent;
import com.dss.mygatherer.controler.event.SucessTypesEvent;
import com.dss.mygatherer.model.CardSet;
import com.dss.mygatherer.model.CardsList;
import com.dss.mygatherer.model.DefaultReturn;
import com.dss.mygatherer.model.Format;
import com.dss.mygatherer.model.Planechase;
import com.dss.mygatherer.model.Price;
import com.dss.mygatherer.model.Set;
import com.dss.mygatherer.model.Type;
import com.dss.mygatherer.model.User;
import com.dss.mygatherer.model.cardsDetails.CardData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class GathererServiceProvider {

    private Retrofit retrofit;
    private static GathererServiceProvider instance;
    String urlBase = "http://mygatherer.xyz/";

    public static GathererServiceProvider getInstance() {
        if (instance == null) {
            synchronized (GathererServiceProvider.class) {
                if (instance == null) {
                    instance = new GathererServiceProvider();
                }
            }
        }
        return instance;
    }

    private Retrofit getRetrofit() {
        if (this.retrofit == null) {

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();


            this.retrofit = new Retrofit.Builder()
                    .baseUrl(urlBase)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build();
        }

        return this.retrofit;
    }

    public void getUser(String login, String password) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<User>> loginData = gathererService.login(login, password);
        loginData.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessLoginEvent(null, false, "Usuário ou senha invalidos"));
                } else {
                    User user = response.body().get(0);
                    EventBus.getDefault().post(new SucessLoginEvent(user, true, ""));
                }
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void getPlanechaseCards() {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<Planechase>> planechaseCards = gathererService.getPlanechaseCards();
        planechaseCards.enqueue(new Callback<List<Planechase>>() {
            @Override
            public void onResponse(Call<List<Planechase>> call, Response<List<Planechase>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessPlanechaseEvent(null, false, "Erro ao buscar cartas"));
                } else {
                    ArrayList<Planechase> cards = new ArrayList<>();
                    cards.addAll(response.body());
                    EventBus.getDefault().post(new SucessPlanechaseEvent(cards, true, ""));
                }
            }

            @Override
            public void onFailure(Call<List<Planechase>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void getUserList(int id) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<CardsList>> cardslist = gathererService.getListCards(id);
        cardslist.enqueue(new Callback<List<CardsList>>() {
            @Override
            public void onResponse(Call<List<CardsList>> call, Response<List<CardsList>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessCardsListEvent(null, false, "Erro ao buscar listas"));
                } else {
                    ArrayList<CardsList> cards = new ArrayList<>();
                    cards.addAll(response.body());
                    EventBus.getDefault().post(new SucessCardsListEvent(cards, true, ""));
                }
            }

            @Override
            public void onFailure(Call<List<CardsList>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void getExpasions(int screen) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<Set>> planechaseCards = gathererService.getAllSets();
        planechaseCards.enqueue(new Callback<List<Set>>() {
            @Override
            public void onResponse(Call<List<Set>> call, Response<List<Set>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessSetsEvent(null, false, "Erro ao buscar Expansões",screen));
                } else {
                    ArrayList<Set> cards = new ArrayList<>();
                    cards.addAll(response.body());
                    EventBus.getDefault().post(new SucessSetsEvent(cards, true, "",screen));
                }
            }

            @Override
            public void onFailure(Call<List<Set>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void getFormats(int screen) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<Format>> planechaseCards = gathererService.getAllFormats();
        planechaseCards.enqueue(new Callback<List<Format>>() {
            @Override
            public void onResponse(Call<List<Format>> call, Response<List<Format>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessFormatEvent(null, false, "Erro ao buscar Formatos",screen));
                } else {
                    ArrayList<Format> cards = new ArrayList<>();
                    cards.addAll(response.body());
                    EventBus.getDefault().post(new SucessFormatEvent(cards, true, "",screen));
                }
            }

            @Override
            public void onFailure(Call<List<Format>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void getCardSets(String set) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<CardSet>> planechaseCards = gathererService.getCardsSet(set);
        planechaseCards.enqueue(new Callback<List<CardSet>>() {
            @Override
            public void onResponse(Call<List<CardSet>> call, Response<List<CardSet>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessCardSetEvent(null, false, "Erro ao buscar Cards"));
                } else {
                    ArrayList<CardSet> cards = new ArrayList<>();
                    cards.addAll(response.body());
                    EventBus.getDefault().post(new SucessCardSetEvent(cards, true, ""));
                }
            }

            @Override
            public void onFailure(Call<List<CardSet>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void getformatsBanned(Format format) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<String>> planechaseCards = gathererService.getBannedsFormat(Integer.valueOf(format.getId_Format()));
        planechaseCards.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {

                if (response.body().isEmpty()) {
                    EventBus.getDefault().post(new SucessBannedListEvent(null, false, "Erro ao buscar Cards", format.getId_Format()));
                } else {
                    ArrayList<String> cards = new ArrayList<>();
                    cards.addAll(response.body());
                    EventBus.getDefault().post(new SucessBannedListEvent(cards, true, "", format.getId_Format()));
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void createUser(String email, String password, String nameuser, String nickname) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<DefaultReturn> planechaseCards = gathererService.createUser(email,password,nameuser,nickname);
        planechaseCards.enqueue(new Callback<DefaultReturn>() {
            @Override
            public void onResponse(Call<DefaultReturn> call, Response<DefaultReturn> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessDefaultEvent(null, false, "Erro ao Criar Usuário"));
                } else {
                    DefaultReturn defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessDefaultEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<DefaultReturn> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void resetPassword(String email) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<DefaultReturn> planechaseCards = gathererService.forgotPassword(email);
        planechaseCards.enqueue(new Callback<DefaultReturn>() {
            @Override
            public void onResponse(Call<DefaultReturn> call, Response<DefaultReturn> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessDefaultEvent(null, false, "Erro ao Autenticar E-mai"));
                } else {
                    DefaultReturn defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessDefaultEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<DefaultReturn> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void resetPassword(int idUser, String oldpass, String newPass) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<DefaultReturn> planechaseCards = gathererService.changePassword(idUser,oldpass,newPass);
        planechaseCards.enqueue(new Callback<DefaultReturn>() {
            @Override
            public void onResponse(Call<DefaultReturn> call, Response<DefaultReturn> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessDefaultEvent(null, false, "Erro ao Alterar Senha"));
                } else {
                    DefaultReturn defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessDefaultEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<DefaultReturn> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void filterCards(HashMap<String, String> params, String colorUrl) {
        GathererService gathererService = getRetrofit().create(GathererService.class);

        StringBuilder s = new StringBuilder();
        s.append(urlBase);
        s.append("api/json/FilterJson.php");
        s.append("?");

        int count = 0;
        int max = params.size();
        boolean hasParams = false;

        for(Map.Entry<String, String> entry : params.entrySet()) {
            count++;
            String key = entry.getKey();
            String value = entry.getValue();

            s.append(key);
            s.append("=");
            s.append(value);
            if(count < max){
                s.append("&");
            }

            hasParams = true;
        }

        if(hasParams && !colorUrl.equals("")){
            s.append("&");
            s.append(colorUrl);
        }else if(!colorUrl.equals("")){
            s.append(colorUrl);
        }

        String url = s.toString();

        Call<List<CardSet>> cards = gathererService.filterResult(url);
        cards.enqueue(new Callback<List<CardSet>>() {
            @Override
            public void onResponse(Call<List<CardSet>> call, Response<List<CardSet>> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessCardsSearchEvent(null, false, "Nenhum Card Encontrado"));
                } else {
                    ArrayList<CardSet> cardsList = new ArrayList<>();
                    cardsList.addAll(response.body());
                    EventBus.getDefault().post(new SucessCardsSearchEvent(cardsList, true, ""));
                }
            }
            @Override
            public void onFailure(Call<List<CardSet>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Nenhum Card Encontrado"));
            }
        });
    }

    public void getTypes() {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<List<Type>> cards = gathererService.getAllTypes();
        cards.enqueue(new Callback<List<Type>>() {
            @Override
            public void onResponse(Call<List<Type>> call, Response<List<Type>> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessTypesEvent(null, false, "Erro ao Buscar Tipos"));
                } else {
                    ArrayList<Type> typeList = new ArrayList<>();
                    typeList.addAll(response.body());
                    EventBus.getDefault().post(new SucessTypesEvent(typeList, true, ""));
                }
            }
            @Override
            public void onFailure(Call<List<Type>> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }

    public void deleteUserList(CardsList list) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<DefaultReturn> planechaseCards = gathererService.deleteList(Integer.parseInt(list.getId_list()));
        planechaseCards.enqueue(new Callback<DefaultReturn>() {
            @Override
            public void onResponse(Call<DefaultReturn> call, Response<DefaultReturn> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessDefaultEvent(null, false, "Erro ao Deletar Lista"));
                } else {
                    DefaultReturn defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessDefaultEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<DefaultReturn> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao Deletar Lista"));
            }
        });
    }

    public void getCardInfo(String card) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<CardData> planechaseCards = gathererService.getCardInfo(card);
        planechaseCards.enqueue(new Callback<CardData>() {
            @Override
            public void onResponse(Call<CardData> call, Response<CardData> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessCardDetailEvent(null, false, "Erro ao buscar carta"));
                } else {
                    CardData defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessCardDetailEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<CardData> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao Buscar Carta"));
            }
        });
    }

    public void getCardPrice(String cardName, String cardSet) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<Price> planechaseCards = gathererService.getCardPrice(cardName,cardSet);
        planechaseCards.enqueue(new Callback<Price>() {
            @Override
            public void onResponse(Call<Price> call, Response<Price> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessCardPriceEvent(null, false, ""));
                } else {
                    Price defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessCardPriceEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<Price> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent(""));
            }
        });
    }

    public void getListDetail(int idCard) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<CardsList> planechaseCards = gathererService.getListDetail(idCard);
        planechaseCards.enqueue(new Callback<CardsList>() {
            @Override
            public void onResponse(Call<CardsList> call, Response<CardsList> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessListDetailEvent(null, false, "Erro ao obter detalhes da lista"));
                } else {
                    CardsList defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessListDetailEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<CardsList> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent(""));
            }
        });
    }

    public void createOrChangeList(String json) {
        GathererService gathererService = getRetrofit().create(GathererService.class);
        Call<DefaultReturn> planechaseCards = gathererService.createOrChangeList(json);
        planechaseCards.enqueue(new Callback<DefaultReturn>() {
            @Override
            public void onResponse(Call<DefaultReturn> call, Response<DefaultReturn> response) {
                if (response.body() == null) {
                    EventBus.getDefault().post(new SucessDefaultEvent(null, false, "Erro ao enviar Lista"));
                } else {
                    DefaultReturn defaultReturn = response.body();
                    EventBus.getDefault().post(new SucessDefaultEvent(defaultReturn, true, ""));
                }
            }

            @Override
            public void onFailure(Call<DefaultReturn> call, Throwable t) {
                EventBus.getDefault().post(new RequestErrorEvent("Erro ao efetuar requisição"));
            }
        });
    }
}
