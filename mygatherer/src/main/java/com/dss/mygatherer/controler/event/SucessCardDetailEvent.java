package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.cardsDetails.CardData;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessCardDetailEvent {
    private CardData cardData;
    private boolean sucess;
    private String message;

    public SucessCardDetailEvent(@Nullable CardData cardData, boolean sucess, String message) {
        this.cardData = cardData;
        this.sucess = sucess;
        this.message = message;
    }

    public CardData getCardData() {
        return cardData;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
