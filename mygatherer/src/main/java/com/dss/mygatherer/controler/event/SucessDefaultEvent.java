package com.dss.mygatherer.controler.event;

import android.support.annotation.Nullable;

import com.dss.mygatherer.model.DefaultReturn;

/**
 * Created by Gustavo Asevedo on 01/11/2017.
 */

public class SucessDefaultEvent {
    private DefaultReturn defaultReturn;
    private boolean sucess;
    private String message;

    public SucessDefaultEvent(@Nullable DefaultReturn defaultReturn, boolean sucess, String message) {
        this.defaultReturn = defaultReturn;
        this.sucess = sucess;
        this.message = message;
    }

    public DefaultReturn getDefaultReturn() {
        return defaultReturn;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
