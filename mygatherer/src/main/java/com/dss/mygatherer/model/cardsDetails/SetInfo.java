package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class SetInfo  implements Serializable{
    private String name_set;

    private String releaseDate_set;

    private String id_set;

    private String code_set;

    public String getName_set() {
        return name_set;
    }

    public void setName_set(String name_set) {
        this.name_set = name_set;
    }

    public String getReleaseDate_set() {
        return releaseDate_set;
    }

    public void setReleaseDate_set(String releaseDate_set) {
        this.releaseDate_set = releaseDate_set;
    }

    public String getId_set() {
        return id_set;
    }

    public void setId_set(String id_set) {
        this.id_set = id_set;
    }

    public String getCode_set() {
        return code_set;
    }

    public void setCode_set(String code_set) {
        this.code_set = code_set;
    }
}
