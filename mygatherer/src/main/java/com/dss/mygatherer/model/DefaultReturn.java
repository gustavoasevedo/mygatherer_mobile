package com.dss.mygatherer.model;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class DefaultReturn {
    private boolean Sucesso;

    private String Mensagem;

    public boolean getSucesso() {
        return Sucesso;
    }

    public void setSucesso(boolean Sucesso) {
        this.Sucesso = Sucesso;
    }

    public String getMensagem() {
        return Mensagem;
    }

    public void setMensagem(String Mensagem) {
        this.Mensagem = Mensagem;
    }
}