package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class FormatLegality implements Serializable{
    private String legality;

    private String format_Name;

    public String getLegality() {
        return legality;
    }

    public void setLegality(String legality) {
        this.legality = legality;
    }

    public String getFormat_Name() {
        return format_Name;
    }

    public void setFormat_Name(String format_Name) {
        this.format_Name = format_Name;
    }
}
