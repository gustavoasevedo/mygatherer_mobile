package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class CardTypes  implements Serializable{
    private String name_Type;

    private String category;

    public String getName_Type() {
        return name_Type;
    }

    public void setName_Type(String name_Type) {
        this.name_Type = name_Type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
