package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class CardData implements Serializable{
    private ArrayList<FormatLegality> Format_Legality;

    private ArrayList<GeneralInfo> General_Info;

    private ArrayList<CardLanguage> Card_language;

    private ArrayList<SetInfo> Set_Info;

    private ArrayList<Flavors> Flavors;

    private ArrayList<Color> Color_Identity;

    private ArrayList<Color> Color;

    private ArrayList<CardTypes> Card_Types;

    public ArrayList<FormatLegality> getFormat_Legality() {
        return Format_Legality;
    }

    public void setFormat_Legality(ArrayList<FormatLegality> Format_Legality) {
        this.Format_Legality = Format_Legality;
    }

    public ArrayList<GeneralInfo> getGeneral_Info() {
        return General_Info;
    }

    public void setGeneral_Info(ArrayList<GeneralInfo> General_Info) {
        this.General_Info = General_Info;
    }

    public ArrayList<CardLanguage> getCard_language() {
        return Card_language;
    }

    public void setCard_language(ArrayList<CardLanguage> Card_language) {
        this.Card_language = Card_language;
    }

    public ArrayList<SetInfo> getSet_Info() {
        return Set_Info;
    }

    public void setSet_Info(ArrayList<SetInfo> Set_Info) {
        this.Set_Info = Set_Info;
    }

    public ArrayList<Flavors> getFlavors() {
        return Flavors;
    }

    public void setFlavors(ArrayList<Flavors> Flavors) {
        this.Flavors = Flavors;
    }

    public ArrayList<Color> getColor_Identity() {
        return Color_Identity;
    }

    public void setColor_Identity(ArrayList<Color> Color_Identity) {
        this.Color_Identity = Color_Identity;
    }

    public ArrayList<Color> getColor() {
        return Color;
    }

    public void setColor(ArrayList<Color> Color) {
        this.Color = Color;
    }

    public ArrayList<CardTypes> getCard_Types() {
        return Card_Types;
    }

    public void setCard_Types(ArrayList<CardTypes> Card_Types) {
        this.Card_Types = Card_Types;
    }
}