package com.dss.mygatherer.model;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class Set implements Serializable {
    private String name_set;

    private String mkm_id_set;

    private String releaseDate_set;

    private String id_set;

    private String mkm_name_set;

    private String type_set;

    private String code_set;

    public Set(String name_set, String id_set) {
        this.name_set = name_set;
        this.id_set = id_set;
    }

    public String getName_set() {
        return name_set;
    }

    public void setName_set(String name_set) {
        this.name_set = name_set;
    }

    public String getMkm_id_set() {
        return mkm_id_set;
    }

    public void setMkm_id_set(String mkm_id_set) {
        this.mkm_id_set = mkm_id_set;
    }

    public String getReleaseDate_set() {
        return releaseDate_set;
    }

    public void setReleaseDate_set(String releaseDate_set) {
        this.releaseDate_set = releaseDate_set;
    }

    public String getId_set() {
        return id_set;
    }

    public void setId_set(String id_set) {
        this.id_set = id_set;
    }

    public String getMkm_name_set() {
        return mkm_name_set;
    }

    public void setMkm_name_set(String mkm_name_set) {
        this.mkm_name_set = mkm_name_set;
    }

    public String getType_set() {
        return type_set;
    }

    public void setType_set(String type_set) {
        this.type_set = type_set;
    }

    public String getCode_set() {
        return code_set;
    }

    public void setCode_set(String code_set) {
        this.code_set = code_set;
    }
}
