package com.dss.mygatherer.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gustavo on 28/02/2018.
 */

public class CardsList implements Serializable{
    private ArrayList<ListCard> ListCard = new ArrayList<>();

    private String name_list = "";

    private String description_list = "";

    private String type_list = "1";

    private String id_list;

    public String getName_list() {
        return name_list;
    }

    public void setName_list(String name_list) {
        this.name_list = name_list;
    }

    public String getDescription_list() {
        return description_list;
    }

    public void setDescription_list(String description_list) {
        this.description_list = description_list;
    }

    public String getType_list() {
        return type_list;
    }

    public void setType_list(String type_list) {
        this.type_list = type_list;
    }

    public String getId_list() {
        return id_list;
    }

    public void setId_list(String id_list) {
        this.id_list = id_list;
    }

    public ArrayList<ListCard> getListCard() {
        return ListCard;
    }

    public void setListCard(ArrayList<ListCard> listCard) {
        ListCard = listCard;
    }
}
