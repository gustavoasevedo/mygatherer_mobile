package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class Color  implements Serializable{
    private String id_Color;

    private String color_Symbol;

    private String color_Name;

    public String getId_Color() {
        return id_Color;
    }

    public void setId_Color(String id_Color) {
        this.id_Color = id_Color;
    }

    public String getColor_Symbol() {
        return color_Symbol;
    }

    public void setColor_Symbol(String color_Symbol) {
        this.color_Symbol = color_Symbol;
    }

    public String getColor_Name() {
        return color_Name;
    }

    public void setColor_Name(String color_Name) {
        this.color_Name = color_Name;
    }
}