package com.dss.mygatherer.model;

import com.dss.sdatabase.annotations.BaseDBFieldName;
import com.dss.sdatabase.annotations.BaseDBPrimaryKey;
import com.dss.sdatabase.annotations.BaseDBType;

/**
 * Created by Gustavo Asevedo on 16/01/2018.
 */

public class Planechase {
    @BaseDBFieldName("text")
    @BaseDBType("TEXT")
    private String text;

    @BaseDBFieldName("language_Card_URL")
    @BaseDBType("TEXT")
    private String language_Card_URL;

    @BaseDBFieldName("name")
    @BaseDBType("TEXT")
    private String name;

    @BaseDBFieldName("artist")
    @BaseDBType("TEXT")
    private String artist;

    @BaseDBFieldName("id_Card")
    @BaseDBType("INTEGER")
    @BaseDBPrimaryKey
    private String id_Card;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLanguage_Card_URL() {
        return language_Card_URL;
    }

    public void setLanguage_Card_URL(String language_Card_URL) {
        this.language_Card_URL = language_Card_URL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getId_Card() {
        return id_Card;
    }

    public void setId_Card(String id_Card) {
        this.id_Card = id_Card;
    }

    @Override
    public String toString() {
        return "ClassPojo [text = " + text + ", language_Card_URL = " + language_Card_URL + ", name = " + name + ", artist = " + artist + ", id_Card = " + id_Card + "]";
    }
}
