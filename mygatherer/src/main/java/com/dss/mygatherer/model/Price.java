package com.dss.mygatherer.model;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class Price {
    private String Price;

    private String Price_Foil;

    public String getPrice() {
        return Price;
    }

    public void setPrice(String Price) {
        this.Price = Price;
    }

    public String getPrice_Foil() {
        return Price_Foil;
    }

    public void setPrice_Foil(String Price_Foil) {
        this.Price_Foil = Price_Foil;
    }

    @Override
    public String toString() {
        return "ClassPojo [Price = " + Price + ", Price_Foil = " + Price_Foil + "]";
    }
}