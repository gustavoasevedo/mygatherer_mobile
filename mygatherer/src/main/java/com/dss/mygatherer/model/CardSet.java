package com.dss.mygatherer.model;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class CardSet implements Serializable {
    private String cmc;

    private String text;

    private String name;

    private String rarity;

    private int id;

    public String getCmc() {
        return cmc;
    }

    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
