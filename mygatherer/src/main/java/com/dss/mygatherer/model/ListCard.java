package com.dss.mygatherer.model;

/**
 * Created by Gustavo Asevedo on 16/06/2018.
 */
public class ListCard {

    private String card_quantity;

    private String text;

    private String name;

    private String type_card_list;

    private String id_card;

    private String id_list;

    public String getCard_quantity ()
    {
        return card_quantity;
    }

    public void setCard_quantity (String card_quantity)
    {
        this.card_quantity = card_quantity;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getType_card_list ()
    {
        return type_card_list;
    }

    public void setType_card_list (String type_card_list)
    {
        this.type_card_list = type_card_list;
    }

    public String getId_card ()
    {
        return id_card;
    }

    public void setId_card (String id_card)
    {
        this.id_card = id_card;
    }

    public String getId_list ()
    {
        return id_list;
    }

    public void setId_list (String id_list)
    {
        this.id_list = id_list;
    }
}
