package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class GeneralInfo implements Serializable{
    private String cmc;

    private String text;

    private String name;

    private String rarity;

    private String id_Card;

    public String getCmc() {
        return cmc;
    }

    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getId_Card() {
        return id_Card;
    }

    public void setId_Card(String id_Card) {
        this.id_Card = id_Card;
    }
}
