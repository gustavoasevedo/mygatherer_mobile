package com.dss.mygatherer.model;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class Type {
    private String name_Type;

    private String category;

    private String id_Type;

    public Type(String name_Type, String category, String id_Type) {
        this.name_Type = name_Type;
        this.category = category;
        this.id_Type = id_Type;
    }

    public String getName_Type() {
        return name_Type;
    }

    public void setName_Type(String name_Type) {
        this.name_Type = name_Type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId_Type() {
        return id_Type;
    }

    public void setId_Type(String id_Type) {
        this.id_Type = id_Type;
    }
}
