package com.dss.mygatherer.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 30/03/2018.
 */
public class Format implements Serializable {
    private String format_Name;

    private String id_Format;

    private ArrayList<String> bannedCards = new ArrayList<>();

    private boolean bannedDownload = false;

    public Format(String format_Name, String id_Format) {
        this.format_Name = format_Name;
        this.id_Format = id_Format;
    }

    public String getFormat_Name() {
        return format_Name;
    }

    public void setFormat_Name(String format_Name) {
        this.format_Name = format_Name;
    }

    public String getId_Format() {
        return id_Format;
    }

    public void setId_Format(String id_Format) {
        this.id_Format = id_Format;
    }

    public ArrayList<String> getBannedCards() {
        return bannedCards;
    }

    public void setBannedCards(ArrayList<String> bannedCards) {
        this.bannedCards = bannedCards;
    }

    public boolean isBannedDownload() {
        return bannedDownload;
    }

    public void setBannedDownload(boolean bannedDownload) {
        this.bannedDownload = bannedDownload;
    }
}