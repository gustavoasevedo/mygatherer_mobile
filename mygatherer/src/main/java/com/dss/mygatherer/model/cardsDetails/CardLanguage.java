package com.dss.mygatherer.model.cardsDetails;

import java.io.Serializable;

/**
 * Created by Gustavo on 13/03/2018.
 */

public class CardLanguage  implements Serializable{
    private String name_set;

    private String name_Card_Language;

    private String language_Card_URL;

    private String language_name;

    public String getName_set() {
        return name_set;
    }

    public void setName_set(String name_set) {
        this.name_set = name_set;
    }

    public String getName_Card_Language() {
        return name_Card_Language;
    }

    public void setName_Card_Language(String name_Card_Language) {
        this.name_Card_Language = name_Card_Language;
    }

    public String getLanguage_Card_URL() {
        return language_Card_URL;
    }

    public void setLanguage_Card_URL(String language_Card_URL) {
        this.language_Card_URL = language_Card_URL;
    }

    public String getLanguage_name() {
        return language_name;
    }

    public void setLanguage_name(String language_name) {
        this.language_name = language_name;
    }
}
