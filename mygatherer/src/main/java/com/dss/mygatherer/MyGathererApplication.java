package com.dss.mygatherer;

import android.support.multidex.MultiDexApplication;

import com.dss.mygatherer.model.CardSet;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

public class MyGathererApplication extends MultiDexApplication {
    private ArrayList<CardSet> cardsLists;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseAnalytics.getInstance(this);
        MobileAds.initialize(this, getApplicationContext().getResources().getString(R.string.BANNER_ID));
    }

    public void setCards(ArrayList<CardSet> cardsLists){
        this.cardsLists = cardsLists;
    }

    public ArrayList<CardSet> getCardsLists(){
        return this.cardsLists;
    }
}
