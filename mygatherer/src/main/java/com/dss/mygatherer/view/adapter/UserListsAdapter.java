package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.dss.mygatherer.R;
import com.dss.mygatherer.model.CardsList;

import java.util.ArrayList;


public class UserListsAdapter extends RecyclerView.Adapter<UserListsAdapter.ViewHolder> implements Filterable {
    Context context;
    int layoutResourceId;
    ArrayList<CardsList> lFilter;
    ArrayList<CardsList> filteredList;
    UserListsListener listener;

    public LayoutInflater inflater;

    public UserListsAdapter(Context context, int textViewResourceId,
                            ArrayList<CardsList> objects, UserListsListener listener) {
        this.context = context;
        this.layoutResourceId = textViewResourceId;
        this.lFilter = objects;
        this.filteredList = objects;
        this.listener = listener;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtNameList;
        TextView txtDescList;
        ImageView imgSee;
        ImageView imgEdit;
        ImageView imgDelete;

        public ViewHolder(View row) {
            super(row);

            txtNameList = row.findViewById(R.id.txtNameList);
            imgSee = row.findViewById(R.id.imgSee);
            imgEdit = row.findViewById(R.id.imgEdit);
            imgDelete = row.findViewById(R.id.imgDelete);
            txtDescList = row.findViewById(R.id.txtDescList);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(layoutResourceId, parent, false);

        UserListsAdapter.ViewHolder vh = new UserListsAdapter.ViewHolder(v);
        return vh;
    }

    public CardsList getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CardsList list = filteredList.get(position);

        holder.txtNameList.setText(list.getName_list());
        holder.txtDescList.setText(list.getDescription_list());
        holder.imgSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.viewList(list);
            }
        });

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.editList(list);
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.deleteList(list);
            }
        });

    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {

                filteredList = (ArrayList<CardsList>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<CardsList> FilteredArrayNames = new ArrayList<>();

                constraint = constraint.toString().toLowerCase();

                for (int i = 0; i < lFilter.size(); i++) {
                    CardsList filter = lFilter.get(i);
                    if (filter.getName_list().toLowerCase().contains(constraint.toString())) {
                        FilteredArrayNames.add(filter);
                    }
                }

                results.count = FilteredArrayNames.size();
                results.values = FilteredArrayNames;

                return results;
            }
        };

        return filter;
    }

}

