package com.dss.mygatherer.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessLoginEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.User;
import com.dss.mygatherer.tools.SharedPrefs;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

    @ViewById
    EditText edtLogin;

    @ViewById
    EditText edtPassword;

    @ViewById
    Button btnLogin;

    @ViewById
    Toolbar tool_bar;

    @ViewById
    TextView txtRegister;

    @ViewById
    TextView txtForgotPass;

    private static int REGISTER_REQUEST = 1;

    private static int RESTET_PASSWORD_REQUEST = 2;

    @AfterViews
    void afterViews() {
        setToolbar();

        txtRegister.setPaintFlags(txtRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtForgotPass.setPaintFlags(txtRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setTitle("Login");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tool_bar.setNavigationOnClickListener(v -> {
            this.finish();
            this.overridePendingTransition(R.anim.slide_out, R.anim.slide_out_finish);
        });
    }

    @Click(R.id.txtRegister)
    void registerClick() {
        startActivityForResult(new Intent(this, RegisterActivity_.class), REGISTER_REQUEST,
                ActivityOptionsCompat.makeCustomAnimation(this, R.anim.slide_in, R.anim.slide_in_finish).toBundle());
    }

    @Click(R.id.txtForgotPass)
    void forgotPassClick() {
        startActivityForResult(new Intent(this, ForgotPasswordActivity_.class), RESTET_PASSWORD_REQUEST,
                ActivityOptionsCompat.makeCustomAnimation(this, R.anim.slide_in, R.anim.slide_in_finish).toBundle());
    }

    @Click(R.id.btnLogin)
    void btnLoginClick() {
        loginUser();
    }

    private void loginUser() {
        ProgressDialog.getInstance(LoginActivity.this).runDialog(false);
        GathererServiceProvider.getInstance().getUser(edtLogin.getText().toString(), edtPassword.getText().toString());

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginSucessfull(SucessLoginEvent sucessLoginEvent) {
        ProgressDialog.getInstance(LoginActivity.this).stopDialog();
        if (sucessLoginEvent.isSucess()) {
            User user = sucessLoginEvent.getUser();
            SharedPrefs.getInstance(this).storeUser(user);

            setResult(RESULT_OK);
            finish();
        } else {
            Toast.makeText(LoginActivity.this, sucessLoginEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(LoginActivity.this).stopDialog();
        Toast.makeText(LoginActivity.this, requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REGISTER_REQUEST) {
                Toast.makeText(LoginActivity.this, "Usuário Criado com Sucesso.", Toast.LENGTH_SHORT).show();
            }else if(requestCode == RESTET_PASSWORD_REQUEST){
                Toast.makeText(LoginActivity.this, "As instruções para sua nova senha foram enviadas no seu e-mail.", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
