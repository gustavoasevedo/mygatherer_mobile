package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dss.mygatherer.R;
import com.dss.mygatherer.model.CardSet;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 14/06/2018.
 */
public class AutoCompleteAdapter extends ArrayAdapter<CardSet> {
    Context context;
    int layoutResourceId;
    ArrayList<CardSet> cards;
    LayoutInflater inflater;

    public AutoCompleteAdapter(Context context, int layoutResourceId, ArrayList<CardSet> cards) {

        super(context, layoutResourceId, cards);

        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.cards = cards;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(layoutResourceId, parent, false);

        CardSet objectItem = cards.get(position);

        TextView textViewItem = convertView.findViewById(R.id.textViewItem);
        textViewItem.setText(objectItem.getName() + "\n\n" + objectItem.getText());


        return convertView;

    }
}
