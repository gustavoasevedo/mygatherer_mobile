package com.dss.mygatherer.view.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import com.dss.mygatherer.MyGathererApplication;
import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessCardDetailEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.CardSet;
import com.dss.mygatherer.model.cardsDetails.CardData;
import com.dss.mygatherer.view.adapter.CardsResultListAdapter;
import com.dss.mygatherer.view.adapter.CardsResultListListener;
import com.dss.mygatherer.view.components.DividerList;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 04/04/2018.
 */

@EActivity(R.layout.activity_search_result)
public class SearchResultActivity extends AppCompatActivity implements CardsResultListListener {

    @ViewById
    RecyclerView listExibition;

    @ViewById
    Toolbar tool_bar;

    ArrayList<CardSet> cards;

    @Extra
    ArrayList<CardData> cardsLists;

    CardsResultListAdapter adapterSet;

    SearchView searchView;

    @AfterViews
    void afterViews() {
        cards =((MyGathererApplication)getApplication()).getCardsLists();
        setToolbar();
        configureAdapter();
    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Resultado da Busca");

        tool_bar.setNavigationOnClickListener(v -> {
            this.finish();
            this.overridePendingTransition(R.anim.slide_out, R.anim.slide_out_finish);
        });

    }

    void configureAdapter() {
            adapterSet = new CardsResultListAdapter(this, R.layout.line_card_search, cards, this);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            listExibition.setLayoutManager(mLayoutManager);

            listExibition.addItemDecoration(new DividerList(ContextCompat.getDrawable(this, R.drawable.custom_divider)), 0);

            listExibition.setAdapter(adapterSet);
            adapterSet.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                    adapterSet.getFilter().filter(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            searchView.setQuery(query, false);
        }
    }

    @Override
    public void getDetailCard(String cardName) {
        ProgressDialog.getInstance(this).runDialog(false);
        GathererServiceProvider.getInstance().getCardInfo(cardName);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSetsSucessfull(SucessCardDetailEvent sucessCardDetailEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        if (sucessCardDetailEvent.isSucess()) {
            Intent i = new Intent(this,CardDetailsActivity_.class);
            i.putExtra("cardData",sucessCardDetailEvent.getCardData());

            startActivity(i,
                    ActivityOptionsCompat.makeCustomAnimation(this, R.anim.slide_in, R.anim.slide_in_finish)
                    .toBundle());
        } else {
            Toast.makeText(this, sucessCardDetailEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCardsFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        Toast.makeText(this, requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
