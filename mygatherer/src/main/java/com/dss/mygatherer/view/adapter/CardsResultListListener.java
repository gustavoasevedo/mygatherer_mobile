package com.dss.mygatherer.view.adapter;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface CardsResultListListener {

    void getDetailCard(String cardName);
}
