package com.dss.mygatherer.view.activity;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessDefaultEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Gustavo Asevedo on 08/05/2018.
 */
@EActivity(R.layout.activity_register)
public class RegisterActivity extends AppCompatActivity {


    @ViewById
    TextInputLayout textInputNickname;

    @ViewById
    EditText edtNickname;

    @ViewById
    TextInputLayout textInputLogin;

    @ViewById
    EditText edtLogin;

    @ViewById
    TextInputLayout textInputPassword;

    @ViewById
    EditText edtPassword;

    @ViewById
    TextInputLayout textInputPasswordConfirm;

    @ViewById
    EditText edtPasswordConfirm;

    @ViewById
    Toolbar tool_bar;

    @ViewById
    Button btnFinish;

    @AfterViews
    void afterViews() {
        setToolbar();
    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setTitle("Cadastro");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tool_bar.setNavigationOnClickListener(v -> {
            this.finish();
            this.overridePendingTransition(R.anim.slide_out, R.anim.slide_out_finish);
        });
    }

    @Click(R.id.btnFinish)
    void finishClick(){
        if(validate()) {
            ProgressDialog.getInstance(this).runDialog(false);
            GathererServiceProvider.getInstance().createUser(edtLogin.getText().toString(), edtPassword.getText().toString(),
                    edtNickname.getText().toString(), edtNickname.getText().toString());
        }
    }

    boolean validate(){
        boolean valid = true;

        if (edtNickname.getText().length() == 0) {
            textInputNickname.requestFocus();
            textInputNickname.setError("É preciso digitar um Nome");
            valid = false;
        } else {
            textInputNickname.setErrorEnabled(false);
        }

        if (edtLogin.getText().length() == 0) {
            textInputLogin.requestFocus();
            textInputLogin.setError("É preciso digitar um Email");
            valid = false;
        } else {
            textInputLogin.setErrorEnabled(false);
        }

        if (edtPassword.getText().length() == 0) {
            textInputPassword.requestFocus();
            textInputPassword.setError("É preciso digitar uma senha");
            valid = false;
        } else {
            textInputPassword.setErrorEnabled(false);
        }

        if (edtPasswordConfirm.getText().length() == 0) {
            textInputPasswordConfirm.requestFocus();
            textInputPasswordConfirm.setError("É preciso digitar uma senha");
            valid = false;
        } else if(!edtPasswordConfirm.getText().toString().equals(edtPassword.getText().toString())) {
            textInputPasswordConfirm.requestFocus();
            textInputPasswordConfirm.setError("As senhas não coincidem");
            valid = false;
        }else {
            textInputPasswordConfirm.setErrorEnabled(false);
        }

        return valid;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCreateUserSucessfull(SucessDefaultEvent sucessCreateUserEvent) {
        ProgressDialog.getInstance(this).stopDialog();

        if (sucessCreateUserEvent.isSucess()) {
            Intent i = new Intent();
            setResult(RESULT_OK,i);
            finish();
        } else {
            Toast.makeText(this, sucessCreateUserEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCreateUserFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        Toast.makeText(this, requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }

}
