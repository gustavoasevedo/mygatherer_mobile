package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.dss.mygatherer.R;
import com.dss.mygatherer.model.Type;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 28/03/2018.
 */

public class TypeSppinerAdapter extends BaseAdapter implements SpinnerAdapter {

    private int resource;
    private LayoutInflater inflater;
    private Context context;
    ArrayList<Type> items;
    ArrayList<String> itemVazio;

    public TypeSppinerAdapter(Context context, int resource, ArrayList<Type> items) {
        super();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        this.context = context;
        this.items = items;
    }

    public TypeSppinerAdapter(Context context, int resource, ArrayList<String> items, Boolean b) {
        super();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
        this.context = context;
        this.itemVazio = items;
    }

    static class ViewHolder {
        public TextView txtTitleLine;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = null;
        ViewHolder viewHolder = null;


        if (convertView == null) {

            view = inflater.inflate(resource, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.txtTitleLine = (TextView) view.findViewById(R.id.txtTitleLine);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            view = convertView;
        }

        Type valor = (Type) getItem(position);
        viewHolder.txtTitleLine.setText(valor.getName_Type());

        return view;
    }
}
