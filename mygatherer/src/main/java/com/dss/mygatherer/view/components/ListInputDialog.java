package com.dss.mygatherer.view.components;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dss.mygatherer.R;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public class ListInputDialog extends DialogFragment {

    String title;
    ListInputDialogListener listener;
    public String inputData;
    public String inputTitleData;
    public EditText edtInput;
    public EditText edtInputTitle;
    String textTitle;
    String textDescription;

    public ListInputDialog() {

    }

    @SuppressLint("ValidFragment")
    public ListInputDialog(String title, String textTitle, String textDescription, ListInputDialogListener listener) {
        super();
        this.title = title;
        this.listener = listener;
        this.textTitle = textTitle;
        this.textDescription = textDescription;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_input_list, null);

        TextView dialogTitle = view.findViewById(R.id.dialogTitle);
        Button btnOk = view.findViewById(R.id.btnOk);
        edtInput = view.findViewById(R.id.edtInput);
        edtInputTitle = view.findViewById(R.id.edtInputTitle);

        dialogTitle.setText(title);

        edtInput.setHint("Descrição");
        edtInputTitle.setHint("Nome da Lista");

        edtInput.setText(textDescription);
        edtInputTitle.setText(textTitle);

        btnOk.setOnClickListener(v -> {
            inputData = edtInput.getText().toString();
            inputTitleData = edtInputTitle.getText().toString();
            listener.onConfirm(ListInputDialog.this);
        });

        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);

        return dialog;
    }
}
