package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dss.mygatherer.R;

import java.util.ArrayList;


public class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder> {
    Context context;
    int layoutResourceId;
    ArrayList<String> lFilter;
    ArrayList<String> filteredList;
    PlayerListListener listener;

    int lifetotal = 20;

    public LayoutInflater inflater;

    public PlayerListAdapter(Context context, int textViewResourceId,
                             ArrayList<String> objects, PlayerListListener listener) {
        this.context = context;
        this.layoutResourceId = textViewResourceId;
        this.lFilter = objects;
        this.filteredList = objects;
        this.listener = listener;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        Button btnDecreaseLife;
        Button btnAddLife;
        Button btnDecreaseInfect;
        Button btnAddInfect;
        TextView txtLife;
        TextView txtPoison;
        TextView txtPlayerName;
        ImageView imgDelete;

        public ViewHolder(View row) {
            super(row);

            btnDecreaseLife = row.findViewById(R.id.btnDecreaseLife);
            btnAddLife = row.findViewById(R.id.btnAddLife);
            btnDecreaseInfect = row.findViewById(R.id.btnDecreaseInfect);
            btnAddInfect = row.findViewById(R.id.btnAddInfect);
            txtLife = row.findViewById(R.id.txtLife);
            txtPoison = row.findViewById(R.id.txtPoison);
            txtPlayerName = row.findViewById(R.id.txtPlayerName);
            imgDelete = row.findViewById(R.id.imgDelete);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(layoutResourceId, parent, false);

        PlayerListAdapter.ViewHolder vh = new PlayerListAdapter.ViewHolder(v);
        return vh;
    }

    public String getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String player = filteredList.get(position);

        holder.txtPlayerName.setText(player);
        holder.txtLife.setText(String.valueOf(lifetotal));

        holder.btnDecreaseLife.setOnClickListener(v -> {
            int life = Integer.valueOf(holder.txtLife.getText().toString());
            life--;
            holder.txtLife.setText(String.valueOf(life));
        });

        holder.btnAddLife.setOnClickListener(v -> {
            int life = Integer.valueOf(holder.txtLife.getText().toString());
            life++;
            holder.txtLife.setText(String.valueOf(life));
        });

        holder.btnDecreaseInfect.setOnClickListener(v -> {
            int infect = Integer.valueOf(holder.txtPoison.getText().toString());
            infect--;
            holder.txtPoison.setText(String.valueOf(infect));
        });

        holder.btnAddInfect.setOnClickListener(v -> {
            int infect = Integer.valueOf(holder.txtPoison.getText().toString());
            infect++;
            holder.txtPoison.setText(String.valueOf(infect));
        });

        holder.imgDelete.setOnClickListener(v -> listener.deleteItem(position));

        holder.txtPlayerName.setOnClickListener(v -> listener.openDialogName(position));

    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    public void setLifeTotal(int life) {
        lifetotal = life;
        notifyDataSetChanged();
    }

    public void setPlayerName(String name, int position) {
        filteredList.set(position, name);
        notifyDataSetChanged();
    }

}

