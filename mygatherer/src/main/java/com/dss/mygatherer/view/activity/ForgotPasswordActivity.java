package com.dss.mygatherer.view.activity;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessDefaultEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Gustavo Asevedo on 14/05/18.
 */

@EActivity(R.layout.activity_forgot_pass)
public class ForgotPasswordActivity extends AppCompatActivity{

    @ViewById
    EditText edtEmail;

    @ViewById
    Button btnSendReset;

    @ViewById
    Toolbar tool_bar;

    @ViewById
    TextInputLayout textInputEmail;

    @AfterViews
    void afterViews(){
        setToolbar();
    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setTitle("Esqueci Minha Senha");
    }

    @Click(R.id.btnSendReset)
    void btnResetPassClick(){
        if(validate()) {
            ProgressDialog.getInstance(this).runDialog(false);
            GathererServiceProvider.getInstance().resetPassword(edtEmail.getText().toString());
        }
    }

    boolean validate(){
        boolean valid = true;

        if (edtEmail.getText().length() == 0) {
            edtEmail.requestFocus();
            edtEmail.setError("É preciso digitar um Email");
            valid = false;
        } else {
            textInputEmail.setErrorEnabled(false);
        }

        return valid;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResetPasswordSucessfull(SucessDefaultEvent sucessResetPasswordEvent) {
        ProgressDialog.getInstance(this).stopDialog();

        if (sucessResetPasswordEvent.isSucess()) {
            Intent i = new Intent();
            setResult(RESULT_OK,i);
            finish();
        } else {
            Toast.makeText(this, sucessResetPasswordEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onResetPasswordFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        Toast.makeText(this, requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
