package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.dss.mygatherer.view.fragment.LifeFragment_;
import com.dss.mygatherer.view.fragment.PlanechaseFragment_;

public class ToolsPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Context mContext;
    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    public ToolsPagerAdapter(FragmentManager fm, int NumOfTabs, Context context) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = new Fragment();
        switch (position) {
            case 0:

                fragment = new LifeFragment_().newInstance();
                break;
            case 1:

                fragment = new PlanechaseFragment_().newInstance();
                break;
        }

        return fragment;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        registeredFragments.remove(position);
        super.destroyItem(container, position, object);
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
