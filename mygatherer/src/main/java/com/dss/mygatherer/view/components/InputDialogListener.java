package com.dss.mygatherer.view.components;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface InputDialogListener {

    void onConfirm(InputDialog dialogFragment);

    void onCancel(InputDialog dialogFragment);
}
