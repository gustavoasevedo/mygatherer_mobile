package com.dss.mygatherer.view.activity;

import android.graphics.Color;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.dss.mygatherer.R;
import com.dss.mygatherer.view.adapter.BottomBarAdapter;
import com.dss.mygatherer.view.components.NoSwipePager;
import com.dss.mygatherer.view.fragment.ConfigFragment;
import com.dss.mygatherer.view.fragment.ListsFragment;
import com.dss.mygatherer.view.fragment.SearchFragment;
import com.dss.mygatherer.view.fragment.ToolsFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById
    AHBottomNavigation bottom_navigation;

    BottomBarAdapter pagerAdapter;

    @ViewById
    NoSwipePager viewPager;

    @ViewById
    Toolbar tool_bar;

    @AfterViews
    void afterViews() {

        setUpBottomBar();

        setToolbar();
    }

    public void setUpBottomBar() {
        setupviewPager();

        bottom_navigation.setBehaviorTranslationEnabled(true);
        bottom_navigation.setTranslucentNavigationEnabled(true);

        bottom_navigation.setDefaultBackgroundColor(Color.WHITE);
        bottom_navigation.setAccentColor(Color.BLACK);
        bottom_navigation.setInactiveColor(fetchColor(R.color.semitransparent));

        bottom_navigation.setColoredModeColors(fetchColor(R.color.highlight_bottom),
                fetchColor(R.color.white));
        bottom_navigation.setColored(true);

        bottom_navigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);

        addItems();

        bottom_navigation.setOnTabSelectedListener((position, wasSelected) -> {
            if (!wasSelected) {
                viewPager.setCurrentItem(position);
                setToolbarLayout(position);
            }
            return true;
        });

        bottom_navigation.setCurrentItem(0);
    }

    public void setupviewPager() {

        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(5);
        pagerAdapter = new BottomBarAdapter(getSupportFragmentManager());

        pagerAdapter.addFragments(SearchFragment.newInstance(R.color.colorPrimary));
        pagerAdapter.addFragments(ToolsFragment.newInstance(R.color.colorPrimary));
        pagerAdapter.addFragments(ListsFragment.newInstance(R.color.colorPrimary));
        pagerAdapter.addFragments(ConfigFragment.newInstance(R.color.colorPrimary));

        viewPager.setAdapter(pagerAdapter);

    }

    private void addItems() {

        AHBottomNavigationItem item1 =
                new AHBottomNavigationItem("Buscar",
                        ContextCompat.getDrawable(this,R.drawable.ic_search),
                        ContextCompat.getColor(this,R.color.colorPrimary));

        AHBottomNavigationItem item2 =
                new AHBottomNavigationItem("Ferramentas",
                        ContextCompat.getDrawable(this,R.drawable.ic_tools),
                        ContextCompat.getColor(this,R.color.colorPrimary));

        AHBottomNavigationItem item3 =
                new AHBottomNavigationItem("Listas",
                        ContextCompat.getDrawable(this,R.drawable.ic_list),
                        ContextCompat.getColor(this,R.color.colorPrimary));

        AHBottomNavigationItem item4 =
                new AHBottomNavigationItem("Configurações",
                        ContextCompat.getDrawable(this,R.drawable.ic_fix),
                        ContextCompat.getColor(this,R.color.colorPrimary));


        bottom_navigation.addItem(item1);
        bottom_navigation.addItem(item2);
        bottom_navigation.addItem(item3);
        bottom_navigation.addItem(item4);

    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);

        setToolbarLayout(0);
    }

    public void setToolbarLayout(int position) {
        tool_bar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        getSupportActionBar().setTitle(bottom_navigation.getItem(position).getTitle(MainActivity.this));

    }

    private int fetchColor(@ColorRes int color) {
        return ContextCompat.getColor(this, color);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(pagerAdapter == null)) {

            pagerAdapter.notifyDataSetChanged();
        }
    }
}
