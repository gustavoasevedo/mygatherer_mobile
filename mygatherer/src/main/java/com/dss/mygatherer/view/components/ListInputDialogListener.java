package com.dss.mygatherer.view.components;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface ListInputDialogListener {

    void onConfirm(ListInputDialog dialogFragment);
}
