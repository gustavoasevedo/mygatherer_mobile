package com.dss.mygatherer.view.activity;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessDefaultEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.tools.SharedPrefs;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Gustavo Asevedo on 08/05/2018.
 */
@EActivity(R.layout.activity_redefine_pass)
public class RedefinePasswordActivity extends AppCompatActivity {

    @ViewById
    TextInputLayout textInputPassword;

    @ViewById
    EditText edtPassword;

    @ViewById
    TextInputLayout textInputNewPassword;

    @ViewById
    EditText edtNewPassword;

    @ViewById
    TextInputLayout textInputPasswordConfirm;

    @ViewById
    EditText edtPasswordConfirm;

    @ViewById
    Toolbar tool_bar;

    @ViewById
    Button btnFinish;

    @AfterViews
    void afterViews() {
        setToolbar();
    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setTitle("Redefinir Senha");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tool_bar.setNavigationOnClickListener(v -> {
            this.finish();
            this.overridePendingTransition(R.anim.slide_out, R.anim.slide_out_finish);
        });
    }

    @Click(R.id.btnFinish)
    void btnFinishClick(){
        if(validate()) {
            ProgressDialog.getInstance(this).runDialog(false);
            GathererServiceProvider.getInstance()
                    .resetPassword(
                            Integer.parseInt(SharedPrefs.getInstance(this).getLoggedUser().getId_user()),
                            edtPassword.getText().toString(),
                            edtNewPassword.getText().toString());
        }
    }

    boolean validate(){
        boolean valid = true;

        if (edtPassword.getText().length() == 0) {
            textInputPassword.requestFocus();
            textInputPassword.setError("É preciso digitar uma Senha");
            valid = false;
        } else {
            textInputPassword.setErrorEnabled(false);
        }

        if (edtNewPassword.getText().length() == 0) {
            textInputNewPassword.requestFocus();
            textInputNewPassword.setError("É preciso digitar uma nova senha");
            valid = false;
        } else if(!edtPasswordConfirm.getText().toString().equals(edtNewPassword.getText().toString())) {
            textInputNewPassword.setErrorEnabled(false);
            textInputPasswordConfirm.requestFocus();
            textInputPasswordConfirm.setError("As novas senhas não coincidem");
            valid = false;
        }else {
            textInputPasswordConfirm.setErrorEnabled(false);
            textInputNewPassword.setErrorEnabled(false);
        }

        return valid;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onsRedefinePasswordSucessfull(SucessDefaultEvent sucessRedefinePasswordEvent) {
        ProgressDialog.getInstance(this).stopDialog();

        if (sucessRedefinePasswordEvent.isSucess()) {
            Intent i = new Intent();
            setResult(RESULT_OK,i);
            finish();
        } else {
            Toast.makeText(this, sucessRedefinePasswordEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onsRedefinePasswordFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        Toast.makeText(this, requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }
}
