package com.dss.mygatherer.view.components;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dss.mygatherer.R;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public class MessageDialog extends DialogFragment {

    String title;
    String message;
    Boolean cancelButton;
    MessageDialogListener listener;

    public MessageDialog() {

    }

    @SuppressLint("ValidFragment")
    public MessageDialog(String title, String message, boolean cancelButton, MessageDialogListener listener) {
        super();
        this.title = title;
        this.message = message;
        this.cancelButton = cancelButton;
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_message, null);

        TextView dialogTitle = view.findViewById(R.id.dialogTitle);
        TextView dialogMessage = view.findViewById(R.id.dialogMessage);
        Button btnOk = view.findViewById(R.id.btnOk);

        Button btnCancel = view.findViewById(R.id.btnCancel);

        dialogTitle.setText(title);
        dialogMessage.setText(message);

        btnOk.setOnClickListener(view12 -> listener.onConfirm(MessageDialog.this));

        btnCancel.setOnClickListener(view1 -> listener.onCancel(MessageDialog.this));

        if (!cancelButton) {
            btnCancel.setVisibility(View.GONE);
        }

        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);

        return dialog;
    }
}
