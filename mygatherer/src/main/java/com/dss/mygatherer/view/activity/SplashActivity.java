package com.dss.mygatherer.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;

import com.dss.mygatherer.R;

/**
 * Created by Gustavo Asevedo on 23/08/2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(this, MainActivity_.class);
        startActivity(intent, ActivityOptionsCompat.makeCustomAnimation(this, R.anim.slide_in, R.anim.slide_in_finish).toBundle());
        finish();
    }
}