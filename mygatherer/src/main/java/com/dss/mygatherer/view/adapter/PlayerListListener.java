package com.dss.mygatherer.view.adapter;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface PlayerListListener {

    void deleteItem(int Position);

    void openDialogName(int position);
}
