package com.dss.mygatherer.view.components;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dss.mygatherer.R;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public class InputDialog extends DialogFragment {

    String title;
    String message;
    Boolean cancelButton;
    String hint;
    InputDialogListener listener;
    public String inputData;
    public EditText edtInput;
    int inputType;


    public InputDialog() {

    }

    @SuppressLint("ValidFragment")
    public InputDialog(String title, String message, boolean cancelButton,
                       String hint, int inputType, String text, InputDialogListener listener) {
        super();
        this.title = title;
        this.message = message;
        this.cancelButton = cancelButton;
        this.hint = hint;
        this.listener = listener;
        this.inputType = inputType;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final FragmentActivity activity = getActivity();

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final LayoutInflater inflater = activity.getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_input, null);

        TextView dialogTitle = view.findViewById(R.id.dialogTitle);
        TextView dialogMessage = view.findViewById(R.id.dialogMessage);
        Button btnOk = view.findViewById(R.id.btnOk);
        edtInput = view.findViewById(R.id.edtInput);

        edtInput.setInputType(inputType);

        Button btnCancel = view.findViewById(R.id.btnCancel);

        dialogTitle.setText(title);
        dialogMessage.setText(message);

        edtInput.setHint(hint);

        btnOk.setOnClickListener(v -> {
            inputData = edtInput.getText().toString();
            listener.onConfirm(InputDialog.this);
        });

        btnCancel.setOnClickListener(view1 -> listener.onCancel(InputDialog.this));

        if (!cancelButton) {
            btnCancel.setVisibility(View.GONE);
        }

        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);

        return dialog;
    }
}
