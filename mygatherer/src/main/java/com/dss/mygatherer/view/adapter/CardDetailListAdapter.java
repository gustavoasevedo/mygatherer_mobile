package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dss.mygatherer.R;
import com.dss.mygatherer.model.ListCard;
import com.dss.mygatherer.view.activity.ListDetailsActivity;

import java.util.ArrayList;


public class CardDetailListAdapter extends RecyclerView.Adapter<CardDetailListAdapter.ViewHolder> {
    Context context;
    int layoutResourceId;
    ArrayList<ListCard> lFilter;
    ArrayList<ListCard> filteredList;
    CardDetailListListener listener;
    int mode;
    public LayoutInflater inflater;

    public CardDetailListAdapter(Context context, int textViewResourceId,
                                 ArrayList<ListCard> objects, CardDetailListListener listener, int mode) {
        this.mode = mode;
        this.context = context;
        this.layoutResourceId = textViewResourceId;
        this.lFilter = objects;
        this.filteredList = objects;
        this.listener = listener;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtDetail;
        TextView txtCardQuantity;
        ImageView imgPlus;
        ImageView imgMinus;
        ImageView imgDelete;

        public ViewHolder(View row) {
            super(row);
            txtName = row.findViewById(R.id.txtName);
            txtDetail = row.findViewById(R.id.txtDetail);
            txtCardQuantity = row.findViewById(R.id.txtCardQuantity);
            imgPlus = row.findViewById(R.id.imgPlus);
            imgMinus = row.findViewById(R.id.imgMinus);
            imgDelete = row.findViewById(R.id.imgDelete);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(layoutResourceId, parent, false);

        CardDetailListAdapter.ViewHolder vh = new CardDetailListAdapter.ViewHolder(v);
        return vh;
    }

    public ListCard getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListCard listCard = filteredList.get(position);

        holder.txtName.setText(listCard.getName());
        holder.txtDetail.setText(listCard.getText());
        holder.txtCardQuantity.setText("Quantidade: "+listCard.getCard_quantity());

        if(mode == ListDetailsActivity.LIST_VISUALIZATION){
            holder.imgPlus.setVisibility(View.GONE);
            holder.imgMinus.setVisibility(View.GONE);
            holder.imgDelete.setVisibility(View.GONE);
        }else{
            holder.imgPlus.setVisibility(View.VISIBLE);
            holder.imgMinus.setVisibility(View.VISIBLE);
            holder.imgDelete.setVisibility(View.VISIBLE);

            holder.imgPlus.setOnClickListener(v -> listener.plusCard(position));
            holder.imgMinus.setOnClickListener(v -> listener.minusCard(position));
            holder.imgDelete.setOnClickListener(v -> listener.deleteCard(position));
        }
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

}

