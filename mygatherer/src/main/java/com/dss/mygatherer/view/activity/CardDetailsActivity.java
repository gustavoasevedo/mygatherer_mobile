package com.dss.mygatherer.view.activity;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.SucessCardPriceEvent;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.cardsDetails.CardData;
import com.dss.mygatherer.model.cardsDetails.CardLanguage;
import com.dss.mygatherer.model.cardsDetails.CardTypes;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Gustavo Asevedo on 05/06/2018.
 */

@EActivity(R.layout.activity_card_details)
public class CardDetailsActivity extends AppCompatActivity {

    @Extra
    CardData cardData;

    @ViewById
    ImageView imgCard;

    @ViewById
    TextView txtTitleCard;

    @ViewById
    TextView txtSubtitle;

    @ViewById
    TextView txtType;

    @ViewById
    TextView txtOracle;

    @ViewById
    TextView txtFlavor;

    @ViewById
    TextView txtPrice;

    @ViewById
    Toolbar tool_bar;

    String portugueseName = "";




    @AfterViews
    void afterViews(){
        setToolbar();
        ProgressDialog.getInstance(this).runDialog(false);
        GathererServiceProvider.getInstance().getCardPrice(cardData.getGeneral_Info().get(0).getName(),
                cardData.getSet_Info().get(0).getCode_set());
        loadData();
    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setTitle("Detalhes do Card");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tool_bar.setNavigationOnClickListener(v -> {
            this.finish();
            this.overridePendingTransition(R.anim.slide_out, R.anim.slide_out_finish);
        });

    }

    void loadData(){

        Glide.with(this)
                .load(getCardUrl())
                .fitCenter()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .animate(R.anim.fade_in)
                .placeholder(R.drawable.default_placeholder)
                .into(imgCard);

        txtTitleCard.setText(cardData.getGeneral_Info().get(0).getName());
        txtSubtitle.setText(portugueseName);

        txtType.setText(getCardType());

        txtOracle.setText(cardData.getGeneral_Info().get(0).getText());

        txtFlavor.setText(cardData.getFlavors().get(0).getFlavor());
    }

    String getCardType(){
        String cardType = "";
        for(CardTypes type : cardData.getCard_Types()){
            if(!type.getCategory().equals("0")){
                cardType += "- ";
            }
            cardType += type.getName_Type() + " ";
        }

        return cardType;
    }

    String getCardUrl(){
        String cardUrl = "";
        String englishUrl = "";

        for(CardLanguage cardLanguage : cardData.getCard_language()){
            if(cardLanguage.getLanguage_name().equals("Portuguese (Brazil)")){
                cardUrl = cardLanguage.getLanguage_Card_URL();
                portugueseName = cardLanguage.getName_Card_Language();

                break;
            }

            if(cardLanguage.getLanguage_name().equals("English")){
                englishUrl = cardLanguage.getLanguage_Card_URL();
            }
        }

        if(cardUrl != ""){
            return cardUrl;
        }else{
            return englishUrl;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSetsSucessfull(SucessCardPriceEvent sucessCardPriceEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        if (sucessCardPriceEvent.isSucess()) {
            txtPrice.setText("R$ " + sucessCardPriceEvent.getPrice().getPrice() +
                    "(Foil: R$ " + sucessCardPriceEvent.getPrice().getPrice_Foil() + ")");
        } else {
            txtPrice.setText("");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCardsFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        txtPrice.setText("");
    }


}
