package com.dss.mygatherer.view.activity;

import android.os.Build;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.async.AutoCompleteAsyncTask;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessDefaultEvent;
import com.dss.mygatherer.controler.event.SucessListDetailEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.CardSet;
import com.dss.mygatherer.model.CardsList;
import com.dss.mygatherer.model.ListCard;
import com.dss.mygatherer.tools.SharedPrefs;
import com.dss.mygatherer.view.adapter.AutoCompleteAdapter;
import com.dss.mygatherer.view.adapter.CardDetailListAdapter;
import com.dss.mygatherer.view.adapter.CardDetailListListener;
import com.dss.mygatherer.view.components.DividerList;
import com.dss.mygatherer.view.components.ListInputDialog;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 13/06/2018.
 */

@EActivity(R.layout.activity_list_detail)
public class ListDetailsActivity extends AppCompatActivity implements CardDetailListListener{

    public static int LIST_VISUALIZATION = 1;
    public static int LIST_EDIT = 2;
    public static int LIST_CREATION = 3;

    @ViewById
    AutoCompleteTextView edtCardName;

    @ViewById
    RecyclerView listPresentation;

    @ViewById
    Button btnConfirmEdit;

    @ViewById
    Toolbar tool_bar;

    @Extra
    int mode;

    @Extra
    CardsList Cardlist;

    CardDetailListAdapter adapter;

    ArrayList<CardSet> listAutoComplete = new ArrayList<>();

    AutoCompleteAsyncTask runningTask;

    AutoCompleteAdapter adapterAutoComplete;

    @AfterViews
    void afterViews(){
        setToolbar();

        switch (mode){
            case 1:
                edtCardName.setVisibility(View.GONE);
                btnConfirmEdit.setVisibility(View.GONE);
                ProgressDialog.getInstance(this).runDialog(false);
                GathererServiceProvider.getInstance().getListDetail(Integer.parseInt(Cardlist.getId_list()));
                break;
            case 2:
                edtCardName.setVisibility(View.VISIBLE);
                btnConfirmEdit.setVisibility(View.VISIBLE);
                configureAutoComplete();
                ProgressDialog.getInstance(this).runDialog(false);
                GathererServiceProvider.getInstance().getListDetail(Integer.parseInt(Cardlist.getId_list()));
                break;
            case 3:
                edtCardName.setVisibility(View.VISIBLE);
                btnConfirmEdit.setVisibility(View.VISIBLE);
                Cardlist = new CardsList();
                configureAutoComplete();
                configureList();
                break;
        }
    }

    public void setToolbar() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tool_bar.setElevation(0);
        }

        setSupportActionBar(tool_bar);
        getSupportActionBar().setTitle("Detalhes da Lista");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tool_bar.setNavigationOnClickListener(v -> {
            this.finish();
            this.overridePendingTransition(R.anim.slide_out, R.anim.slide_out_finish);
        });
    }

    void configureList(){
        adapter = new CardDetailListAdapter(this, R.layout.line_list_detail, Cardlist.getListCard(),
                this,mode);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        listPresentation.setLayoutManager(mLayoutManager);

        listPresentation.addItemDecoration(new DividerList(ContextCompat.getDrawable(this, R.drawable.custom_divider)), 0);

        listPresentation.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Click(R.id.btnConfirmEdit)
    void  btnConfirmClick(){
        ListInputDialog inputDialog = new ListInputDialog("Detalhes da Lista",
                Cardlist.getName_list(),
                Cardlist.getDescription_list(),
                dialogFragment -> {

                    if (!dialogFragment.inputData.isEmpty()) {
                        Cardlist.setName_list(dialogFragment.inputTitleData);
                    }

                    if (!dialogFragment.inputTitleData.isEmpty()) {
                        Cardlist.setDescription_list(dialogFragment.inputData);
                    }


                    try {
                        String jsonsend = buildJson();
                        ProgressDialog.getInstance(this).runDialog(false);
                        GathererServiceProvider.getInstance().createOrChangeList(jsonsend);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



                    dialogFragment.dismiss();
                });

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        inputDialog.show(ft, "rate_prompt");
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void on(SucessDefaultEvent defaultReturn) {
        ProgressDialog.getInstance(this).stopDialog();

        if (defaultReturn.isSucess()) {
            Toast.makeText(this, "Lista Atualizada com sucesso!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, defaultReturn.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void on(SucessListDetailEvent sucessListDetailEvent) {
        ProgressDialog.getInstance(this).stopDialog();

        if (sucessListDetailEvent.isSucess()) {
            this.Cardlist = sucessListDetailEvent.getCardsList();
            configureList();
        } else {
            Toast.makeText(this, sucessListDetailEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(this).stopDialog();
        Toast.makeText(this, requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }

    void configureAutoComplete(){
        adapterAutoComplete = new AutoCompleteAdapter(
                this, R.layout.line_text_autocomplete, listAutoComplete);
        edtCardName.setAdapter(adapterAutoComplete);

        edtCardName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(runningTask != null) {
                    runningTask.cancel(true);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                runningTask = new AutoCompleteAsyncTask(ListDetailsActivity.this, s.toString(),
                        cards -> {
                    if(cards != null) {
                        listAutoComplete = new ArrayList<>();
                        listAutoComplete.addAll(cards);
                        adapterAutoComplete = new AutoCompleteAdapter(
                                ListDetailsActivity.this,
                                R.layout.line_text_autocomplete, listAutoComplete);

                        edtCardName.setAdapter(adapterAutoComplete);
                        adapterAutoComplete.notifyDataSetChanged();
                        runningTask = null;
                    }else {
                        listAutoComplete = new ArrayList<>();
                        adapterAutoComplete = new AutoCompleteAdapter(
                                ListDetailsActivity.this,
                                R.layout.line_text_autocomplete, listAutoComplete);

                        edtCardName.setAdapter(adapterAutoComplete);
                        adapterAutoComplete.notifyDataSetChanged();
                        runningTask = null;
                    }
                        });


                runningTask.execute();
            }
        });

        edtCardName.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                CardSet selected = (CardSet) arg0.getAdapter().getItem(arg2);
                ListCard listCard = new ListCard();
                listCard.setCard_quantity("1");
                listCard.setText(selected.getText());
                listCard.setName(selected.getName());
                listCard.setType_card_list(Cardlist.getType_list());
                listCard.setId_card(String.valueOf(selected.getId()));
                listCard.setId_list(String.valueOf(Cardlist.getId_list()));

                Cardlist.getListCard().add(listCard);
                adapter.notifyDataSetChanged();

                edtCardName.setText("");
            }
        });
    }

    @Override
    public void plusCard(int position) {
        int quantity = Integer.parseInt(Cardlist.getListCard().get(position).getCard_quantity());
        Cardlist.getListCard().get(position).setCard_quantity(String.valueOf(quantity + 1));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void minusCard(int position) {
        int quantity = Integer.parseInt(Cardlist.getListCard().get(position).getCard_quantity());
        Cardlist.getListCard().get(position).setCard_quantity(String.valueOf(quantity - 1));

        adapter.notifyDataSetChanged();
    }

    @Override
    public void deleteCard(int position) {
        Cardlist.getListCard().remove(position);

        adapter.notifyDataSetChanged();
    }

    String buildJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name_list",Cardlist.getName_list());
        jsonObject.put("type_list",Integer.valueOf(Cardlist.getType_list()));
        jsonObject.put("description_list",Cardlist.getDescription_list());
        jsonObject.put("id_user",Integer.valueOf(SharedPrefs.getInstance(this).getLoggedUser().getId_user()));
        jsonObject.put("type_list",1);
        jsonObject.put("status",1);

        if(Cardlist.getId_list() != null && Cardlist.getId_list() != ""){
            jsonObject.put("id_list",Integer.valueOf(Cardlist.getId_list()));
        }

        JSONArray jsonArray = new JSONArray();

        for(ListCard listCard : Cardlist.getListCard()){
            JSONObject jsonInside = new JSONObject();
            jsonInside.put("id_card",Integer.valueOf(listCard.getId_card()));
            jsonInside.put("type_card_list",listCard.getType_card_list());
            jsonInside.put("card_quantity",listCard.getCard_quantity());

            jsonArray.put(jsonInside);
        }

        jsonObject.put("list_cards",jsonArray);

        return jsonObject.toString();
    }
}
