package com.dss.mygatherer.view.adapter;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface CardDetailListListener {

    void plusCard(int Position);

    void minusCard(int position);

    void deleteCard(int position);
}
