package com.dss.mygatherer.view.adapter;

import com.dss.mygatherer.model.CardsList;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface UserListsListener {

    void deleteList(CardsList list);

    void viewList (CardsList list);

    void editList(CardsList list);
}
