package com.dss.mygatherer.view.components;

/**
 * Created by Gustavo Asevedo on 11/12/2017.
 */

public interface MessageDialogListener {

    void onConfirm(MessageDialog dialogFragment);

    void onCancel(MessageDialog dialogFragment);
}
