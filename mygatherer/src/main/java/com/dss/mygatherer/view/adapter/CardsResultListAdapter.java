package com.dss.mygatherer.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.dss.mygatherer.R;
import com.dss.mygatherer.model.CardSet;
import com.dss.mygatherer.model.cardsDetails.CardData;

import java.util.ArrayList;


public class CardsResultListAdapter extends RecyclerView.Adapter<CardsResultListAdapter.ViewHolder> implements Filterable {
    Context context;
    int layoutResourceId;
    ArrayList<CardSet> lFilter;
    ArrayList<CardSet> filteredList;

    ArrayList<CardData> lFilterSearch;
    ArrayList<CardData> filteredListSearch;
    CardsResultListListener listener;

    public boolean isMainSearch = false;

    public LayoutInflater inflater;

    public CardsResultListAdapter(Context context, int textViewResourceId,
                                  ArrayList<CardSet> objects,CardsResultListListener listener) {
        this.context = context;
        this.layoutResourceId = textViewResourceId;
        this.lFilter = objects;
        this.filteredList = objects;
        this.listener = listener;
        isMainSearch = false;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public CardsResultListAdapter(Context context, int textViewResourceId
                                  , CardsResultListListener listener,ArrayList<CardData> objects) {
        this.context = context;
        this.layoutResourceId = textViewResourceId;
        this.lFilterSearch = objects;
        this.filteredListSearch = objects;
        this.listener = listener;
        isMainSearch = true;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtCardName;
        TextView txtRarity;
        TextView txtCmc;
        TextView txtCardText;

        public ViewHolder(View row) {
            super(row);

            txtCardName = row.findViewById(R.id.txtCardName);
            txtRarity = row.findViewById(R.id.txtRarity);
            txtCmc = row.findViewById(R.id.txtCmc);
            txtCardText = row.findViewById(R.id.txtCardText);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(layoutResourceId, parent, false);

        CardsResultListAdapter.ViewHolder vh = new CardsResultListAdapter.ViewHolder(v);
        return vh;
    }

    public Object getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(isMainSearch) {
            CardData cardData = filteredListSearch.get(position);

            holder.itemView.setOnClickListener(v -> {
                listener.getDetailCard(cardData.getGeneral_Info().get(0).getName());
            });
            holder.txtCardName.setText(cardData.getGeneral_Info().get(0).getName());
            holder.txtRarity.setText(cardData.getGeneral_Info().get(0).getRarity());
            holder.txtCmc.setText("CMC: " + cardData.getGeneral_Info().get(0).getCmc());
            holder.txtCardText.setText(cardData.getGeneral_Info().get(0).getText());

        }else{
            CardSet card = filteredList.get(position);

            holder.itemView.setOnClickListener(v -> {
                listener.getDetailCard(card.getName());
            });
            holder.txtCardName.setText(card.getName());
            holder.txtRarity.setText(card.getRarity());
            holder.txtCmc.setText("CMC: " + card.getCmc());
            holder.txtCardText.setText(card.getText());
        }

    }

    @Override
    public int getItemCount() {
        if(isMainSearch){
            return filteredListSearch.size();
        }else{
            return filteredList.size();
        }

    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                if(isMainSearch){
                    filteredListSearch = (ArrayList<CardData>) results.values;
                }else{
                    filteredList = (ArrayList<CardSet>) results.values;
                }

                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                if(isMainSearch){
                    FilterResults results = new FilterResults();
                    ArrayList<CardData> FilteredArrayNames = new ArrayList<>();

                    constraint = constraint.toString().toLowerCase();

                    for (int i = 0; i < lFilterSearch.size(); i++) {
                        CardData filter = lFilterSearch.get(i);
                        if (filter.getGeneral_Info().get(0).getName().toLowerCase().contains(constraint.toString())) {
                            FilteredArrayNames.add(filter);
                        }
                    }

                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;

                    return results;
                }else {

                    FilterResults results = new FilterResults();
                    ArrayList<CardSet> FilteredArrayNames = new ArrayList<>();

                    constraint = constraint.toString().toLowerCase();

                    for (int i = 0; i < lFilter.size(); i++) {
                        CardSet filter = lFilter.get(i);
                        if (filter.getName().toLowerCase().contains(constraint.toString())) {
                            FilteredArrayNames.add(filter);
                        }
                    }

                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;

                    return results;
                }
            }
        };

        return filter;
    }

}

