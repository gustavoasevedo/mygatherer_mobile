package com.dss.mygatherer.view.components;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.dss.mygatherer.R;
import com.wang.avi.AVLoadingIndicatorView;

/**
 * Created by Gustavo Asevedo on 29/08/2017.
 */

public class ProgressDialog {

    private static ProgressDialog instance;
    private Context context;
    AlertDialog alertName;
    AVLoadingIndicatorView avLoadingIndicatorViewbuilder;

    Boolean isShowing = false;

    private ProgressDialog(Context context) {
        this.context = context;
    }

    public static ProgressDialog getInstance(Context newContext) {
        if (instance == null) {
            synchronized (ProgressDialog.class) {
                if (instance == null) {
                    instance = new ProgressDialog(newContext);
                } else if (newContext != instance.context) {
                    instance = new ProgressDialog(newContext);
                }
            }
        } else if (newContext != instance.context) {
            instance = new ProgressDialog(newContext);
        }
        return instance;
    }

    public void runDialog(boolean cancelable) {

        LayoutInflater factory = LayoutInflater
                .from(context);

        final View textEntryView = factory.inflate(R.layout.dialog_progress,
                null);

        AlertDialog.Builder builder;

        if (!isShowing) {
            try {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    builder = new AlertDialog.Builder(context, R.style.progressDialogWebmotors);
                } else {
                    builder = new AlertDialog.Builder(context);
                }

                AlertDialog.Builder builderName = builder;

                builderName
                        .setCancelable(cancelable)
                        .setTitle("")
                        .setView(textEntryView);

                alertName = builderName.create();

                avLoadingIndicatorViewbuilder = textEntryView.findViewById(R.id.progressSpin);
                avLoadingIndicatorViewbuilder.smoothToShow();

                alertName.show();

                isShowing = true;

                Window window = alertName.getWindow();
                window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

            } catch (Exception e) {
                System.gc();
            }
        }
    }

    public void stopDialog() {
        if (isShowing) {
            try {
                avLoadingIndicatorViewbuilder.hide();
                alertName.dismiss();
                alertName = null;
                instance = null;
                isShowing = false;

            } catch (Exception e) {
                System.gc();
            }
        }
    }

    public Dialog getProgress() {
        return alertName;
    }

}
