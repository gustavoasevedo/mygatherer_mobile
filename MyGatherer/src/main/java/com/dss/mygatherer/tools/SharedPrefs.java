package com.dss.mygatherer.tools;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.dss.mygatherer.model.User;


public class SharedPrefs {
    private SharedPreferences sharedPreferences = null;
    private Context context;
    private static SharedPrefs instance;

    private SharedPrefs(Context ctx) {
        this.context = ctx.getApplicationContext();
        this.sharedPreferences = ctx.getSharedPreferences(ConstantSharedPrefs.PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static SharedPrefs getInstance(Context context) {
        if (instance == null) {
            synchronized (SharedPrefs.class) {
                if (instance == null) {
                    instance = new SharedPrefs(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    @Override
    protected void finalize() throws Throwable {
        this.sharedPreferences = null;
        super.finalize();
    }

    public void clearSharedPreferences() {
        Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public void storePlanechaseSaved(Boolean saved) {
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(ConstantSharedPrefs.PLANECHASE_SAVED, saved);
        editor.apply();
    }

    public Boolean isPlanechaseSaved() {
        Boolean isSaved = sharedPreferences.getBoolean(ConstantSharedPrefs.PLANECHASE_SAVED, false);
        return isSaved;
    }


    public void storeUser(User user) {
        Editor editor = sharedPreferences.edit();
        editor.putBoolean(ConstantSharedPrefs.IS_USER_LOGGED, true);
        editor.putString(ConstantSharedPrefs.USER_LOGGED_ID, user.getId_user());
        editor.putString(ConstantSharedPrefs.USER_LOGGED_NAME, user.getName_user());
        editor.putString(ConstantSharedPrefs.USER_LOGGED_NICK, user.getNickname());
        editor.putString(ConstantSharedPrefs.USER_LOGGED_EMAIL, user.getEmail());
        editor.apply();
    }

    public User getLoggedUser() {
        User user = new User();
        user.setId_user(sharedPreferences.getString(ConstantSharedPrefs.USER_LOGGED_ID, ""));
        user.setName_user(sharedPreferences.getString(ConstantSharedPrefs.USER_LOGGED_NAME, ""));
        user.setNickname(sharedPreferences.getString(ConstantSharedPrefs.USER_LOGGED_NICK, ""));
        user.setEmail(sharedPreferences.getString(ConstantSharedPrefs.USER_LOGGED_EMAIL, ""));
        return user;
    }

    public Boolean isUserLogged() {
        Boolean isUserLogged = sharedPreferences.getBoolean(ConstantSharedPrefs.IS_USER_LOGGED, false);
        return isUserLogged;
    }

    public void logOutUser() {
        Editor editor = sharedPreferences.edit();
        editor.remove(ConstantSharedPrefs.IS_USER_LOGGED);
        editor.remove(ConstantSharedPrefs.USER_LOGGED_ID);
        editor.remove(ConstantSharedPrefs.USER_LOGGED_NAME);
        editor.remove(ConstantSharedPrefs.USER_LOGGED_NICK);
        editor.remove(ConstantSharedPrefs.USER_LOGGED_EMAIL);
        editor.apply();
    }

}
