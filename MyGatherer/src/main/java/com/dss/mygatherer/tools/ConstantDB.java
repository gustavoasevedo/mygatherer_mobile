package com.dss.mygatherer.tools;

/**
 * Created by Gustavo Asevedo on 17/01/2018.
 */

public class ConstantDB {

    public static String DB_NAME = "MY_GATHERER_DB";
    public static int DB_VERSION = 1;
}
