package com.dss.mygatherer.tools;

/**
 * Created by Gustavo Asevedo on 17/01/2018.
 */

public class ConstantSharedPrefs {

    public static String PREFERENCES_NAME = "MY_GATHERER_PREFERENCES";

    public static String PLANECHASE_SAVED = "PLANECHASE_SAVED_PREFERENCES";

    public static String IS_USER_LOGGED = "IS_USER_LOGGED_PREFERENCES";
    public static String USER_LOGGED_ID = "USER_LOGGED_ID_PREFERENCES";
    public static String USER_LOGGED_NAME = "USER_LOGGED_NAME_PREFERENCES";
    public static String USER_LOGGED_NICK = "USER_LOGGED_NICK_PREFERENCES";
    public static String USER_LOGGED_EMAIL = "USER_LOGGED_EMAIL_PREFERENCES";
}
