package com.dss.mygatherer.view.fragment;

import android.app.Activity;import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.dss.mygatherer.BuildConfig;
import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.PlanechaseDao;
import com.dss.mygatherer.model.User;
import com.dss.mygatherer.tools.SharedPrefs;
import com.dss.mygatherer.view.activity.LoginActivity_;
import com.dss.mygatherer.view.activity.RedefinePasswordActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Gustavo Asevedo on 13/04/2017.
 */

@EFragment(R.layout.fragment_config)
public class ConfigFragment extends Fragment {

    @FragmentArg("color")
    int color;

    @ViewById
    ConstraintLayout containterUser;

    @ViewById
    Button btnRedefinePass;

    @ViewById
    Button btnLogOut;

    @ViewById
    ImageView ic_letter;

    @ViewById
    TextView txtName;

    @ViewById
    TextView txtEmail;

    @ViewById
    TextView txtAppVersion;

    @ViewById
    Button btnClearData;

    @ViewById
    Button btnLogin;

    private static int LOGIN_CONFIG_REQUEST = 3;
    private static int REDEFINE_PASSWORD_CONFIG_REQUEST = 4;

    public static ConfigFragment_ newInstance(int color) {
        ConfigFragment_ f = new ConfigFragment_();
        Bundle args = new Bundle();
        args.putSerializable("color", color);
        f.setArguments(args);
        return f;
    }

    @AfterViews
    void afterViews() {
        if(SharedPrefs.getInstance(getActivity()).isUserLogged()){
            containterUser.setVisibility(View.VISIBLE);
            btnRedefinePass.setVisibility(View.VISIBLE);
            btnLogOut.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.GONE);

            loadUserData();
        }else{
            containterUser.setVisibility(View.GONE);
            btnRedefinePass.setVisibility(View.GONE);
            btnLogOut.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);
        }

        txtAppVersion.setText("Versão do Aplicativo: " + BuildConfig.VERSION_NAME);
    }

    @Override
    public void onResume(){
        super.onResume();
        afterViews();
    }


    void loadUserData(){
        User user = SharedPrefs.getInstance(getActivity()).getLoggedUser();

        TextDrawable letter = TextDrawable.builder()
                .buildRoundRect(String.valueOf(user.getName_user().charAt(0)),
                        getActivity().getResources().getColor(R.color.colorPrimary),
                        20);

        ic_letter.setImageDrawable(letter);
        txtName.setText(user.getName_user());
        txtEmail.setText(user.getEmail());
    }

    @Click(R.id.btnLogOut)
    void btnLogOutClick(){
        SharedPrefs.getInstance(getActivity()).logOutUser();
        afterViews();
    }

    @Click(R.id.btnClearData)
    void btnEraseDataClick() {
        showMessageOKCancel();
    }

    @Click(R.id.btnRedefinePass)
    void btnRedefinePassClick(){
        startActivityForResult(new Intent(getActivity(), RedefinePasswordActivity_.class), REDEFINE_PASSWORD_CONFIG_REQUEST,
                ActivityOptionsCompat.makeCustomAnimation(getActivity(), R.anim.slide_in, R.anim.slide_in_finish).toBundle());
    }

    @Click(R.id.btnLogin)
    void btnLogin(){
        startActivityForResult(new Intent(getActivity(), LoginActivity_.class), LOGIN_CONFIG_REQUEST,
                ActivityOptionsCompat.makeCustomAnimation(getActivity(), R.anim.slide_in, R.anim.slide_in_finish).toBundle());
    }

    public void showMessageOKCancel() {
        AlertDialog.Builder builder;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Light_Dialog);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }

        builder.setMessage("Ao apagar os dados armazenados no aplicativo serão apagados. Você tem certeza que deseja apagar os dados?")
                .setPositiveButton("OK", (dialog, which) -> {
                    clearPreferences();
                    eraseDatabase();

                    afterViews();

                    Toast.makeText(getActivity(), "Dados apagados com sucesso.", Toast.LENGTH_SHORT).show();
                    })
                .setNegativeButton("Cancel", null)
                .create()
                .show();

    }

    public void clearPreferences() {
        SharedPrefs.getInstance(getActivity()).clearSharedPreferences();
    }

    public void eraseDatabase() {
        PlanechaseDao.getInstance(getActivity()).deleteAllItems();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == LOGIN_CONFIG_REQUEST) {
                afterViews();
            }else if(requestCode == REDEFINE_PASSWORD_CONFIG_REQUEST){
                Toast.makeText(getActivity(), "Sucesso ao redefinir senha", Toast.LENGTH_LONG).show();
            }
        }
    }
}
