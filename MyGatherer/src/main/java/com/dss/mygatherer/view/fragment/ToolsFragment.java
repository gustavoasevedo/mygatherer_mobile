package com.dss.mygatherer.view.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dss.mygatherer.R;
import com.dss.mygatherer.view.adapter.ToolsPagerAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.Random;

/**
 * Created by Gustavo Asevedo on 13/04/2017.
 */

@EFragment(R.layout.fragment_tools)
public class ToolsFragment extends Fragment {

    @FragmentArg("color")
    int color;

    @ViewById
    Button btnCoin;

    @ViewById
    Button btnDice20;

    @ViewById
    Button btnDice6;

    @ViewById
    ConstraintLayout background;

    ToolsPagerAdapter pagerAdapter;

    @ViewById
    TabLayout tab_layout;

    @ViewById
    ViewPager pager;

    public static ToolsFragment_ newInstance(int color) {
        ToolsFragment_ f = new ToolsFragment_();
        Bundle args = new Bundle();
        args.putSerializable("color", color);
        f.setArguments(args);
        return f;
    }

    @AfterViews
    void afterViews() {
        configureTabs();
        configureAdapter();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!(pagerAdapter == null)) {

            pagerAdapter.notifyDataSetChanged();
        }
    }

    public void configureAdapter() {
        pagerAdapter = new ToolsPagerAdapter(getChildFragmentManager(), tab_layout.getTabCount(), getActivity());

        pager.setAdapter(pagerAdapter);
    }

    public void configureTabs() {
        tab_layout.addTab(tab_layout.newTab().setText("CONTADOR DE VIDA"));
        tab_layout.addTab(tab_layout.newTab().setText("PLANECHASE"));

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout));
        tab_layout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                ViewGroup vg = (ViewGroup) tab_layout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTypeface(Typeface.DEFAULT_BOLD);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                ViewGroup vg = (ViewGroup) tab_layout.getChildAt(0);
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(tab.getPosition());
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTypeface(Typeface.DEFAULT);
                    }
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (3 * scale + 0.5f);
        tab_layout.setSelectedTabIndicatorHeight(dpAsPixels);
    }

    @Click(R.id.btnCoin)
    void btnCoinClick() {
        final int min = 1;
        final int max = 2;
        Random r = new Random();
        int random = r.nextInt((max - min) + 1) + min;

        if (random == 1) {
            Toast.makeText(getActivity(), "Cara", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Coroa", Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.btnDice6)
    void btnDice6Click() {
        final int min = 1;
        final int max = 6;
        Random r = new Random();
        int random = r.nextInt((max - min) + 1) + min;

        Toast.makeText(getActivity(), String.valueOf(random), Toast.LENGTH_SHORT).show();
    }

    @Click(R.id.btnDice20)
    void btnDice20Click() {
        final int min = 1;
        final int max = 20;
        Random r = new Random();
        int random = r.nextInt((max - min) + 1) + min;

        Toast.makeText(getActivity(), String.valueOf(random), Toast.LENGTH_SHORT).show();
    }

}
