package com.dss.mygatherer.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dss.mygatherer.MyGathererApplication;
import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessCardsSearchEvent;
import com.dss.mygatherer.controler.event.SucessFormatEvent;
import com.dss.mygatherer.controler.event.SucessSetsEvent;
import com.dss.mygatherer.controler.event.SucessTypesEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.Format;
import com.dss.mygatherer.model.Set;
import com.dss.mygatherer.model.Type;
import com.dss.mygatherer.view.activity.SearchResultActivity_;
import com.dss.mygatherer.view.adapter.FormatsSppinerAdapter;
import com.dss.mygatherer.view.adapter.SetSppinerAdapter;
import com.dss.mygatherer.view.adapter.TypeSppinerAdapter;
import com.dss.mygatherer.view.components.ProgressDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Gustavo Asevedo on 13/04/2017.
 */

@EFragment(R.layout.fragment_filter)
public class SearchFragment extends Fragment {

    @FragmentArg("color")
    int color;

    @ViewById
    ConstraintLayout background;

    @ViewById
    Spinner spnType;

    ArrayList<Type> listTypes;

    TypeSppinerAdapter adapterType;

    @ViewById
    Spinner spnFormats;

    ArrayList<Format> listFormats;

    FormatsSppinerAdapter adapterFormats;

    @ViewById
    Spinner spnSets;

    ArrayList<Set> listSets;

    SetSppinerAdapter adapterSets;

    @ViewById
    TextInputLayout textInputCardName;

    @ViewById
    EditText edtCardName;

    @ViewById
    CheckBox checkWhite;

    @ViewById
    CheckBox checkBlue;

    @ViewById
    CheckBox checkBlack;

    @ViewById
    CheckBox checkRed;

    @ViewById
    CheckBox checkGreen;

    @ViewById
    Button btnSearchCards;

    HashMap<String, String> params = new HashMap<>();
    String colorURL;

    public static SearchFragment_ newInstance(int color) {
        SearchFragment_ f = new SearchFragment_();
        Bundle args = new Bundle();
        args.putSerializable("color", color);
        f.setArguments(args);
        return f;
    }

    @AfterViews
    void afterViews() {
        GathererServiceProvider.getInstance().getFormats(1);
        GathererServiceProvider.getInstance().getExpasions(1);
        GathererServiceProvider.getInstance().getTypes();
    }

    @Click(R.id.btnSearchCards)
    void btnSearchClick(){
        try {
            params = new HashMap<>();
            verifyChecks();
            verifyText();
            verifySpinners();

            if (params.size() > 0 || !colorURL.equals("")) {
                ProgressDialog.getInstance(getActivity()).runDialog(false);
                GathererServiceProvider.getInstance().filterCards(params, colorURL);
            } else {
                Toast.makeText(getActivity(), "É preciso escolher ao menos uma opção no filtro de Busca",
                        Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Toast.makeText(getActivity(), "Falha ao realizar busca",
                    Toast.LENGTH_SHORT).show();
        }
    }


    void verifyChecks(){
        colorURL = "";

        boolean hasPrevious = false;

        if(checkWhite.isChecked()){
            colorURL+= "color[]=" + "W";
            hasPrevious = true;
        }
        if(checkBlue.isChecked()){
            if(hasPrevious){
                colorURL+="&";
            }else{
                hasPrevious = true;
            }
            colorURL+= "color[]=" + "U";
        }
        if(checkBlack.isChecked()){
            if(hasPrevious){
                colorURL+="&";
            }else{
                hasPrevious = true;
            }
            colorURL+= "color[]=" + "B";
        }
        if(checkRed.isChecked()){
            if(hasPrevious){
                colorURL+="&";
            }else{
                hasPrevious = true;
            }

            colorURL+= "color[]=" + "R";
        }
        if(checkGreen.isChecked()){
            if(hasPrevious){
                colorURL+="&";
            }
            colorURL+= "color[]=" + "G";
        }
    }

    void verifyText(){
        String cardName = edtCardName.getText().toString();

        if(cardName.length() > 0){
            params.put("name",cardName);
        }
    }

    void verifySpinners(){
        if(!((Type)spnType.getSelectedItem()).getName_Type().equals("Selecione um Tipo de Card")){
            params.put("type[]",((Type)spnType.getSelectedItem()).getName_Type());
        }

        if(!((Format)spnFormats.getSelectedItem()).getFormat_Name().equals("Selecione um Formato")){
            params.put("format[]",((Format)spnFormats.getSelectedItem()).getFormat_Name());
        }

        if(!((Set)spnSets.getSelectedItem()).getName_set().equals("Selecione uma Edição")){
            params.put("set[]",((Set)spnSets.getSelectedItem()).getCode_set());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSearchCardsSucessfull(SucessCardsSearchEvent sucessCardsSearchEvent) {
        ProgressDialog.getInstance(getActivity()).stopDialog();
        if (sucessCardsSearchEvent.isSucess()) {

            Intent i = new Intent(getActivity(),SearchResultActivity_.class);
            ((MyGathererApplication)getActivity().getApplication()).setCards(sucessCardsSearchEvent.getCardsLists());
            i.putExtra("isMainSearch",false);

            startActivity(i,
                    ActivityOptionsCompat.makeCustomAnimation(getActivity(), R.anim.slide_in, R.anim.slide_in_finish)
                            .toBundle());
        } else {
            Toast.makeText(getActivity(), sucessCardsSearchEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTypesSucessfull(SucessTypesEvent sucessTypesEvent) {
        ProgressDialog.getInstance(getActivity()).stopDialog();
        if (sucessTypesEvent.isSucess()) {
            listTypes = new ArrayList<>();
            listTypes.add(new Type("Selecione um Tipo de Card","","0"));
            listTypes.addAll(sucessTypesEvent.getCardsLists());
            configureTypeSpinner();
        } else {
            Toast.makeText(getActivity(), sucessTypesEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void configureTypeSpinner(){
        adapterType = new TypeSppinerAdapter(getActivity(),
                R.layout.line_spinner_sell, listTypes);
        spnType.setAdapter(adapterType);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFormatSucessfull(SucessFormatEvent sucessFormatEvent) {
        if(sucessFormatEvent.getScreen() == 1) {
            ProgressDialog.getInstance(getActivity()).stopDialog();
            if (sucessFormatEvent.isSucess()) {
                listFormats = new ArrayList<>();
                listFormats.add(new Format("Selecione um Formato","0"));
                listFormats.addAll(sucessFormatEvent.getFormats());
                configureFormatsSpinner();
            } else {
                Toast.makeText(getActivity(), sucessFormatEvent.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    void configureFormatsSpinner(){
        adapterFormats = new FormatsSppinerAdapter(getActivity(),
                R.layout.line_spinner_sell, listFormats);
        spnFormats.setAdapter(adapterFormats);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSetsSucessfull(SucessSetsEvent sucessSetsEvent) {
        if(sucessSetsEvent.getScreen() == 1) {
            ProgressDialog.getInstance(getActivity()).stopDialog();
            if (sucessSetsEvent.isSucess()) {
                listSets = new ArrayList<>();
                listSets.add(new Set("Selecione uma Edição","0"));
                listSets.addAll(sucessSetsEvent.getSets());
                configureSetsSpinner();
            } else {
                Toast.makeText(getActivity(), sucessSetsEvent.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    void configureSetsSpinner(){
        adapterSets = new SetSppinerAdapter(getActivity(),
                R.layout.line_spinner_sell, listSets);
        spnSets.setAdapter(adapterSets);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlanechaseFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(getActivity()).stopDialog();
        Toast.makeText(getActivity(), requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }

}
