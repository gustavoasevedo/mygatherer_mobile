package com.dss.mygatherer.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.widget.Button;

import com.dss.mygatherer.R;
import com.dss.mygatherer.view.adapter.PlayerListAdapter;
import com.dss.mygatherer.view.adapter.PlayerListListener;
import com.dss.mygatherer.view.components.DividerList;
import com.dss.mygatherer.view.components.InputDialog;
import com.dss.mygatherer.view.components.InputDialogListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 13/04/2017.
 */

@EFragment(R.layout.fragment_life)
public class LifeFragment extends Fragment implements PlayerListListener {

    @ViewById
    RecyclerView listPlayers;

    @ViewById
    Button btnAddPlayer;

    @ViewById
    Button btnSetLife;

    PlayerListAdapter adapter;

    ArrayList<String> players = new ArrayList<>();

    int lifeTotal;

    public static LifeFragment_ newInstance() {
        LifeFragment_ f = new LifeFragment_();
        return f;
    }

    @AfterViews
    void afterViews() {
        configureAdapter();
    }

    void configureAdapter() {
        adapter = new PlayerListAdapter(getActivity(), R.layout.line_player_list, players, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        listPlayers.setLayoutManager(mLayoutManager);

        listPlayers.addItemDecoration(new DividerList(ContextCompat.getDrawable(getActivity(), R.drawable.custom_divider)), 0);

        listPlayers.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Click(R.id.btnAddPlayer)
    void btnAddClick() {
        String playername = "Jogador " + String.valueOf(players.size() + 1);
        players.add(playername);
        adapter.notifyDataSetChanged();
    }

    @Click(R.id.btnSetLife)
    void btnLifeClick() {
        InputDialog inputDialog = new InputDialog("Configuração",
                "Digite a vida inicial (Atenção: Isso ira resetar o total de vida do jogo atual)",
                true,
                "Vida",
                InputType.TYPE_CLASS_NUMBER,
                String.valueOf(lifeTotal),
                new InputDialogListener() {
                    @Override
                    public void onConfirm(InputDialog dialogFragment) {
                        if (!dialogFragment.inputData.isEmpty()) {
                            lifeTotal = Integer.valueOf(dialogFragment.inputData);
                            adapter.setLifeTotal(lifeTotal);
                            adapter.notifyDataSetChanged();
                        }

                        dialogFragment.dismiss();
                    }

                    @Override
                    public void onCancel(InputDialog dialogFragment) {
                        dialogFragment.dismiss();
                    }
                });

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        inputDialog.show(ft, "rate_prompt");
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void deleteItem(int Position) {
        players.remove(Position);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void openDialogName(int position) {
        InputDialog inputDialog = new InputDialog("Configuração",
                "Digite o nome do Jogador",
                true,
                "Nome do Jogador",
                InputType.TYPE_TEXT_VARIATION_PERSON_NAME,
                adapter.getItem(position),
                new InputDialogListener() {
                    @Override
                    public void onConfirm(InputDialog dialogFragment) {
                        if (!dialogFragment.inputData.isEmpty()) {
                            String name = dialogFragment.inputData;
                            adapter.setPlayerName(name, position);
                            adapter.notifyDataSetChanged();
                        }

                        dialogFragment.dismiss();
                    }

                    @Override
                    public void onCancel(InputDialog dialogFragment) {
                        dialogFragment.dismiss();
                    }
                });
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        inputDialog.show(ft, "rate_prompt");
    }
}
