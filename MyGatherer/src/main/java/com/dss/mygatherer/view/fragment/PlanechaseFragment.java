package com.dss.mygatherer.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.PlanechaseDao;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessPlanechaseEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.Planechase;
import com.dss.mygatherer.tools.SharedPrefs;
import com.dss.mygatherer.view.components.MessageDialog;
import com.dss.mygatherer.view.components.MessageDialogListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Created by Gustavo Asevedo on 13/04/2017.
 */

@EFragment(R.layout.fragment_planechase)
public class PlanechaseFragment extends Fragment {

    @ViewById
    Button btnPlanechase;

    @ViewById
    ProgressBar progressPlanchase;

    @ViewById
    ImageView cardDisplay;

    @ViewById
    Button btnNextPlane;

    @ViewById
    Button btnShuffle;

    @ViewById
    Button btnPlanarDice;

    ArrayList<Planechase> planechases;

    int planechasePosition = 0;

    public static PlanechaseFragment_ newInstance() {
        PlanechaseFragment_ f = new PlanechaseFragment_();
        return f;
    }

    @AfterViews
    void afterViews() {

        if (SharedPrefs.getInstance(getActivity()).isPlanechaseSaved()) {
            btnPlanechase.setVisibility(View.GONE);
            btnNextPlane.setVisibility(View.VISIBLE);
            btnShuffle.setVisibility(View.VISIBLE);

            startPlanechase();

        } else {
            btnPlanechase.setVisibility(View.VISIBLE);
            btnNextPlane.setVisibility(View.GONE);
            btnShuffle.setVisibility(View.GONE);
        }

    }

    void startPlanechase() {
        planechases = PlanechaseDao.getInstance(getActivity()).selectAllCards();

        planechasePosition = 0;

        Collections.shuffle(planechases);
        Collections.shuffle(planechases);
        Collections.shuffle(planechases);

        Glide.with(getActivity())
                .load(planechases.get(0).getLanguage_Card_URL())
                .placeholder(getActivity().getResources().getDrawable(R.drawable.default_placeholder))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .animate(R.anim.fade_in)
                .into(cardDisplay);
    }

    @Click(R.id.btnShuffle)
    void btnShuffle() {
        MessageDialog messageDialog = new MessageDialog("Atenção", "Deseja Realmente Re-Embaralhar os Planos?",
                true, new MessageDialogListener() {
            @Override
            public void onConfirm(MessageDialog dialogFragment) {
                startPlanechase();
                dialogFragment.dismiss();
            }

            @Override
            public void onCancel(MessageDialog dialogFragment) {
                dialogFragment.dismiss();

            }
        });

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        messageDialog.show(ft, "rate_prompt");


    }

    @Click(R.id.btnNextPlane)
    void btnNextPlane() {
        planechasePosition++;

        if (planechasePosition > planechases.size()) {
            planechasePosition = 0;
        }


        Glide.with(getActivity())
                .load(planechases.get(planechasePosition).getLanguage_Card_URL())
                .placeholder(getActivity().getResources().getDrawable(R.drawable.default_placeholder))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .animate(R.anim.fade_in)
                .into(cardDisplay);

    }

    @Click(R.id.btnPlanechase)
    void btnPlanechaseClick() {
        progressPlanchase.setIndeterminate(true);
        progressPlanchase.setVisibility(View.VISIBLE);
        GathererServiceProvider.getInstance().getPlanechaseCards();
    }

    @Click(R.id.btnPlanarDice)
    void btnPlanarDiceClick() {
        final int min = 1;
        final int max = 6;
        Random r = new Random();
        int random = r.nextInt((max - min) + 1) + min;

        switch (random) {
            case 1:
            case 2:
            case 3:
            case 4:
                Toast.makeText(getActivity(), "Tente Novamente!", Toast.LENGTH_SHORT).show();
                break;
            case 5:
                Toast.makeText(getActivity(), "CAOS!", Toast.LENGTH_SHORT).show();
                break;
            case 6:
                Toast.makeText(getActivity(), "TRANSPLANAR!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlanechaseSucessfull(SucessPlanechaseEvent sucessPlanechaseEvent) {

        if (sucessPlanechaseEvent.isSucess()) {
            ArrayList<Planechase> planechases = sucessPlanechaseEvent.getPlanechases();

            progressPlanchase.setIndeterminate(false);
            progressPlanchase.setMax(planechases.size());
            progressPlanchase.setProgress(0);

            for (Planechase planechase : planechases) {
                PlanechaseDao.getInstance(getActivity()).insertObject(planechase);
                progressPlanchase.setProgress(progressPlanchase.getProgress() + 1);
            }

            SharedPrefs.getInstance(getActivity()).storePlanechaseSaved(true);
            btnPlanechase.setVisibility(View.GONE);
            progressPlanchase.setVisibility(View.GONE);
            btnNextPlane.setVisibility(View.VISIBLE);
            btnShuffle.setVisibility(View.VISIBLE);

            startPlanechase();
        } else {
            progressPlanchase.setVisibility(View.GONE);
            Toast.makeText(getActivity(), sucessPlanechaseEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlanechaseFailed(RequestErrorEvent requestErrorEvent) {

        progressPlanchase.setIndeterminate(true);
        progressPlanchase.setVisibility(View.GONE);

        Toast.makeText(getActivity(), requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }
}