package com.dss.mygatherer.view.fragment;

import android.app.Activity;import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dss.mygatherer.BuildConfig;
import com.dss.mygatherer.R;
import com.dss.mygatherer.controler.event.RequestErrorEvent;
import com.dss.mygatherer.controler.event.SucessCardsListEvent;
import com.dss.mygatherer.controler.event.SucessDefaultEvent;
import com.dss.mygatherer.controler.service.GathererServiceProvider;
import com.dss.mygatherer.model.CardsList;
import com.dss.mygatherer.tools.SharedPrefs;
import com.dss.mygatherer.view.activity.ListDetailsActivity;
import com.dss.mygatherer.view.activity.ListDetailsActivity_;
import com.dss.mygatherer.view.activity.LoginActivity_;
import com.dss.mygatherer.view.adapter.UserListsAdapter;
import com.dss.mygatherer.view.adapter.UserListsListener;
import com.dss.mygatherer.view.components.DividerList;
import com.dss.mygatherer.view.components.MessageDialog;
import com.dss.mygatherer.view.components.MessageDialogListener;
import com.dss.mygatherer.view.components.ProgressDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

/**
 * Created by Gustavo Asevedo on 13/04/2017.
 */

@EFragment(R.layout.fragment_lists)
public class ListsFragment extends Fragment implements UserListsListener {

    @FragmentArg("color")
    int color;

    @ViewById
    ConstraintLayout background;

    @ViewById
    TextView txtTitle;

    @ViewById
    Button btnLogin;

    @ViewById
    Button btnAddList;

    @ViewById
    AdView adView;

    @ViewById
    RecyclerView list;

    @ViewById
    TextView txtPlaceholder;

    ArrayList<CardsList> cardsLists;

    UserListsAdapter adapter;

    private static int LOGIN_LIST_REQUEST = 1;

    public static ListsFragment_ newInstance(int color) {
        ListsFragment_ f = new ListsFragment_();
        Bundle args = new Bundle();
        args.putSerializable("color", color);
        f.setArguments(args);
        return f;
    }

    @AfterViews
    void afterViews() {
        if(BuildConfig.FLAVOR.equals("free")) {
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }

        if (SharedPrefs.getInstance(getActivity()).isUserLogged()) {
            txtTitle.setVisibility(View.GONE);
            btnLogin.setVisibility(View.GONE);
            btnAddList.setVisibility(View.VISIBLE);
            int idUser = Integer.parseInt(SharedPrefs.getInstance(getActivity()).getLoggedUser().getId_user());

            GathererServiceProvider.getInstance().getUserList((idUser));
        } else {
            txtTitle.setVisibility(View.VISIBLE);
            btnLogin.setVisibility(View.VISIBLE);
            btnAddList.setVisibility(View.GONE);
            txtPlaceholder.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        afterViews();
    }


    @Click(R.id.btnLogin)
    void btnLoginClick() {
        startActivityForResult(new Intent(getActivity(), LoginActivity_.class), LOGIN_LIST_REQUEST,
                ActivityOptionsCompat.makeCustomAnimation(getActivity(), R.anim.slide_in, R.anim.slide_in_finish).toBundle());
    }

    @Click(R.id.btnAddList)
    void addListClick(){
        Intent i = new Intent(getActivity(),ListDetailsActivity_.class);
        i.putExtra("mode", ListDetailsActivity.LIST_CREATION);
        startActivity(i);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == LOGIN_LIST_REQUEST) {
                afterViews();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCardsListSucessfull(SucessCardsListEvent sucessCardsListEvent) {
        ProgressDialog.getInstance(getActivity()).stopDialog();

        if (sucessCardsListEvent.isSucess()) {
            cardsLists = sucessCardsListEvent.getCardsLists();
            if(cardsLists.size() > 0) {
                txtPlaceholder.setVisibility(View.GONE);
                configureAdapter();
            }else{
                configureEmpty();
            }

        } else {
            ProgressDialog.getInstance(getActivity()).stopDialog();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDeleteSucessfull(SucessDefaultEvent sucessDefaultEvent) {
        ProgressDialog.getInstance(getActivity()).stopDialog();

        if (sucessDefaultEvent.isSucess()) {

        } else {
            Toast.makeText(getActivity(), sucessDefaultEvent.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFailed(RequestErrorEvent requestErrorEvent) {
        ProgressDialog.getInstance(getActivity()).stopDialog();
        Toast.makeText(getActivity(), requestErrorEvent.getMessage(), Toast.LENGTH_SHORT).show();
    }

    void configureAdapter() {
        adapter = new UserListsAdapter(getActivity(), R.layout.line_list_home, cardsLists, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        list.setLayoutManager(mLayoutManager);

        list.addItemDecoration(new DividerList(ContextCompat.getDrawable(getActivity(), R.drawable.custom_divider)), 0);

        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    void configureEmpty(){
        list.setVisibility(View.GONE);
        txtPlaceholder.setVisibility(View.VISIBLE);
    }

    @Override
    public void deleteList(CardsList list) {
        MessageDialog messageDialog = new MessageDialog("Atenção", "Deseja Realmente Deletar a lista?",
                true, new MessageDialogListener() {
            @Override
            public void onConfirm(MessageDialog dialogFragment) {
                ProgressDialog.getInstance(getActivity()).runDialog(false);
                GathererServiceProvider.getInstance().deleteUserList(list);
                cardsLists.remove(list);
                adapter.notifyDataSetChanged();
                dialogFragment.dismiss();
            }

            @Override
            public void onCancel(MessageDialog dialogFragment) {
                dialogFragment.dismiss();

            }
        });

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        messageDialog.show(ft, "rate_prompt");
    }

    @Override
    public void viewList(CardsList list) {
        Intent i = new Intent(getActivity(),ListDetailsActivity_.class);
        i.putExtra("Cardlist",list);
        i.putExtra("mode", ListDetailsActivity.LIST_VISUALIZATION);
        startActivity(i);
    }

    @Override
    public void editList(CardsList list) {
        Intent i = new Intent(getActivity(),ListDetailsActivity_.class);
        i.putExtra("Cardlist",list);
        i.putExtra("mode", ListDetailsActivity.LIST_EDIT);
        startActivity(i);
    }
}
